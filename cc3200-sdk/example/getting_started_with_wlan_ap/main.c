//*****************************************************************************
//
// Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/ 
// 
// 
//  Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
//    Redistributions of source code must retain the above copyright 
//    notice, this list of conditions and the following disclaimer.
//
//    Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the 
//    documentation and/or other materials provided with the   
//    distribution.
//
//    Neither the name of Texas Instruments Incorporated nor the names of
//    its contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*****************************************************************************


//*****************************************************************************
//
// Application Name     - Getting started with WLAN AP
// Application Overview - This application aims to exhibit the CC3200 device as
//                        AP. Developers/users can refer the function or re-use 
//                        them while writing new application.
// Application Details  -
// http://processors.wiki.ti.com/index.php/CC32xx_Getting_Started_with_WLAN_AP
// or
// docs\examples\CC32xx_Getting_Started_with_WLAN_AP.pdf
//
//*****************************************************************************


//****************************************************************************
//
//! \addtogroup getting_started_ap
//! @{
//
//****************************************************************************

#include <stdlib.h>
#include <string.h>

// Simplelink includes
#include "simplelink.h"

// driverlib includes 
#include "hw_types.h"
#include "hw_ints.h"
#include "rom.h"
#include "rom_map.h"
#include "interrupt.h"
#include "prcm.h"
#include "utils.h"

// free_rtos/ti-rtos includes 
#include "osi.h"

// common interface includes
#include "common.h"
#ifndef NOTERM
#include "uart_if.h"
#endif
#include "pinmux.h"

#define APP_NAME                "WLAN AP"
#define APPLICATION_VERSION     "1.1.0"
#define OSI_STACK_SIZE          2048


//
// Values for below macros shall be modified for setting the 'Ping' properties
//
#define PING_INTERVAL       1000    /* In msecs */
#define PING_TIMEOUT        3000    /* In msecs */
#define PING_PKT_SIZE       20      /* In bytes */
#define NO_OF_ATTEMPTS      3
#define PING_FLAG           0
#define P2P_REMOTE_DEVICE   "remote-p2p-device"
#define P2P_DEVICE_NAME     "cc3200-p2p-device"
#define P2P_CONFIG_VALUE    "1-0050F204-1"
#define P2P_SECURITY_KEY    ""
#define P2P_SECURITY_TYPE   SL_SEC_TYPE_P2P_PBC
#define LISENING_CHANNEL    11
#define REGULATORY_CLASS    81
#define OPERATING_CHANNEL   6
#define BUF_SIZE            1400
#define TCP_PACKET_COUNT    1
#define PORT_NUM            5001      /* Port to be used  by TCP server*/
#define Delay(x)            MAP_UtilsDelay(x * 8000)


#define P2P_ROLE_TYPE_NEGOTIATE

// Application specific status/error codes

typedef enum{
    // Choosing this number to avoid overlap w/ host-driver's error codes 
    NETWORK_CONNECTION_FAILED = -0x7D0,
    P2P_CONFIG_FAILED = NETWORK_CONNECTION_FAILED - 1,
    P2P_MODE_START_FAILED = P2P_CONFIG_FAILED - 1,
    DEVICE_NOT_IN_STATION_MODE = P2P_MODE_START_FAILED - 1,
    CLIENT_DISCONNECTED = DEVICE_NOT_IN_STATION_MODE -1,

    STATUS_CODE_MAX = -0xBB8
}e_AppStatusCodes;
union
{
    char BsdBuf[BUF_SIZE];
    unsigned long demobuf[BUF_SIZE/4];
} uBuf;

//*****************************************************************************
//                 GLOBAL VARIABLES -- Start
//*****************************************************************************
unsigned char  g_ulStatus = 0;
unsigned long  g_ulStaIp = 0;
unsigned long  g_ulPingPacketsRecv = 0;
unsigned long  g_ulGatewayIP = 0;
unsigned long  g_ulDeviceIp = 0;
unsigned long  g_ulPacketCount = TCP_PACKET_COUNT;
char g_p2p_dev[MAXIMAL_SSID_LENGTH + 1];
char g_cBsdBuf[BUF_SIZE];

#if defined(gcc)
extern void (* const g_pfnVectors[])(void);
#endif
#if defined(ewarm)
extern uVectorEntry __vector_table;
#endif
//*****************************************************************************
//                 GLOBAL VARIABLES -- End
//*****************************************************************************



//****************************************************************************
//                      LOCAL FUNCTION PROTOTYPES
//****************************************************************************
static int PingTest(unsigned long ulIpAddr);
static long ConfigureSimpleLinkToDefaultState();
static void InitializeAppVariables();
static int GetSsidName(char *pcSsidName, unsigned int uiMaxLen);



#ifdef USE_FREERTOS
//*****************************************************************************
// FreeRTOS User Hook Functions enabled in FreeRTOSConfig.h
//*****************************************************************************

//*****************************************************************************
//
//! \brief Application defined hook (or callback) function - assert
//!
//! \param[in]  pcFile - Pointer to the File Name
//! \param[in]  ulLine - Line Number
//! 
//! \return none
//!
//*****************************************************************************
void
vAssertCalled( const char *pcFile, unsigned long ulLine )
{
    //Handle Assert here
    while(1)
    {
    }
}

//*****************************************************************************
//
//! \brief Application defined idle task hook
//! 
//! \param  none
//! 
//! \return none
//!
//*****************************************************************************
void
vApplicationIdleHook( void)
{
    //Handle Idle Hook for Profiling, Power Management etc
}

//*****************************************************************************
//
//! \brief Application defined malloc failed hook
//! 
//! \param  none
//! 
//! \return none
//!
//*****************************************************************************
void vApplicationMallocFailedHook()
{
    //Handle Memory Allocation Errors
    while(1)
    {
    }
}

//*****************************************************************************
//
//! \brief Application defined stack overflow hook
//! 
//! \param  none
//! 
//! \return none
//!
//*****************************************************************************
void vApplicationStackOverflowHook(OsiTaskHandle *pxTask, 
                                   signed char *pcTaskName)
{
    //Handle FreeRTOS Stack Overflow
    while(1)
    {
    }
}
#endif //USE_FREERTOS

static int SomeTcpClient(unsigned short usPort)
{
    UART_PRINT("SomeTcpClient started \n\r");
    int             iCounter;
    short           sTestBufLen;
    SlSockAddrIn_t  sAddr;
    int             iAddrSize;
    int             iSockID;
    int             iStatus;
    long            lLoopCount = 0;
    char            l_string_msg[] = "Hello TCP World!";
    char            l_msg_len = strlen(l_string_msg);

    // filling the buffer
    /*
    for (iCounter=0 ; iCounter<BUF_SIZE ; iCounter++)
    {
        g_cBsdBuf[iCounter] = (char)(iCounter % 10);
    }
    */

    sTestBufLen  = BUF_SIZE;

    //filling the TCP server socket address
    sAddr.sin_family = SL_AF_INET;
    sAddr.sin_port = sl_Htons((unsigned short)usPort);
    //sAddr.sin_addr.s_addr = sl_Htonl((unsigned int)g_ulDestinationIp);
    sAddr.sin_addr.s_addr = sl_Htonl((unsigned int)g_ulGatewayIP);

    iAddrSize = sizeof(SlSockAddrIn_t);

    UART_PRINT("Creating TCP socket\r\n");
    // creating a TCP socket
    iSockID = sl_Socket(SL_AF_INET,SL_SOCK_STREAM, 0);
    if( iSockID < 0 )
    {
        ASSERT_ON_ERROR(NETWORK_CONNECTION_FAILED);
    }

    UART_PRINT("Connecting to TCP server\r\n");
    // connecting to TCP server
    iStatus = sl_Connect(iSockID, ( SlSockAddr_t *)&sAddr, iAddrSize);
    if( iStatus < 0 )
    {
        // error
        ASSERT_ON_ERROR(sl_Close(iSockID));
        ASSERT_ON_ERROR(NETWORK_CONNECTION_FAILED);
    }

    UART_PRINT("Sending 1 packet to TCP server\r\n");
    UART_PRINT("Message is: %s\r\n", l_string_msg);
    UART_PRINT("Message length: %d\r\n", l_msg_len);

    while (lLoopCount < g_ulPacketCount)
    {
        // sending packet
        iStatus = sl_Send(iSockID, l_string_msg, l_msg_len, 0 );
        if( iStatus <= 0 )
        {
            // error
            ASSERT_ON_ERROR(sl_Close(iSockID));
            ASSERT_ON_ERROR(NETWORK_CONNECTION_FAILED);
        }
        lLoopCount++;
    }

    Report("Sent %u packets successfully\n\r",g_ulPacketCount);

    //closing the socket after sending 1000 packets
    ASSERT_ON_ERROR(sl_Close(iSockID));

    return SUCCESS;
}

//*****************************************************************************
//
//! On Successful completion of Wlan Connect, This function triggers Connection
//! status to be set. 
//!
//! \param  pSlWlanEvent pointer indicating Event type
//!
//! \return None
//!
//*****************************************************************************
void SimpleLinkWlanEventHandler(SlWlanEvent_t *pSlWlanEvent)
{ 
    switch(pSlWlanEvent->Event)
    {
        case SL_WLAN_CONNECT_EVENT:
        {
            SET_STATUS_BIT(g_ulStatus, STATUS_BIT_CONNECTION);
            CLR_STATUS_BIT(g_ulStatus, STATUS_BIT_CONNECTION_FAILED);

            //
            // Information about the connected AP (like name, MAC etc) will be
            // available in 'slWlanConnectAsyncResponse_t' - Applications
            // can use it if required
            //
            //  slWlanConnectAsyncResponse_t *pEventData = NULL;
            // pEventData = &pSlWlanEvent->EventData.STAandP2PModeWlanConnected;
            //
            //
        }
        break;

        case SL_WLAN_DISCONNECT_EVENT:
        {
            slWlanConnectAsyncResponse_t*  pEventData = NULL;

            CLR_STATUS_BIT(g_ulStatus, STATUS_BIT_CONNECTION);
            CLR_STATUS_BIT(g_ulStatus, STATUS_BIT_IP_AQUIRED);

            pEventData = &pSlWlanEvent->EventData.STAandP2PModeDisconnected;

            // If the user has initiated 'Disconnect' request, 'reason_code'
            // is SL_USER_INITIATED_DISCONNECTION
            if(SL_USER_INITIATED_DISCONNECTION == pEventData->reason_code)
            {
                UART_PRINT("Device disconnected from the Peer Device on "
                                 "application's request \n\r");
            }
            else
            {
                UART_PRINT("Device disconnected from the Peer device on an ERROR..!! \n\r");
            }
        }
        break;

        case SL_WLAN_STA_CONNECTED_EVENT:
        {
            // when device is in AP mode and any client connects to device cc3xxx
            SET_STATUS_BIT(g_ulStatus, STATUS_BIT_CONNECTION);
            CLR_STATUS_BIT(g_ulStatus, STATUS_BIT_CONNECTION_FAILED);

            //
            // Information about the connected client (like SSID, MAC etc) will
            // be available in 'slPeerInfoAsyncResponse_t' - Applications
            // can use it if required
            //
            // slPeerInfoAsyncResponse_t *pEventData = NULL;
            // pEventData = &pSlWlanEvent->EventData.APModeStaConnected;
            //

        }
        break;

        case SL_WLAN_STA_DISCONNECTED_EVENT:
        {
            // when client disconnects from device (AP)
            CLR_STATUS_BIT(g_ulStatus, STATUS_BIT_CONNECTION);
            CLR_STATUS_BIT(g_ulStatus, STATUS_BIT_IP_LEASED);

            //
            // Information about the connected client (like SSID, MAC etc) will
            // be available in 'slPeerInfoAsyncResponse_t' - Applications
            // can use it if required
            //
            // slPeerInfoAsyncResponse_t *pEventData = NULL;
            // pEventData = &pSlWlanEvent->EventData.APModestaDisconnected;
            //
        }
        break;


        case SL_WLAN_P2P_DEV_FOUND_EVENT:
        {
            SET_STATUS_BIT(g_ulStatus, STATUS_BIT_P2P_DEV_FOUND);
            //
            // Information about P2P config details (like Peer device name, own
            // SSID etc) will be available in 'slPeerInfoAsyncResponse_t' -
            // Applications can use it if required
            //
            // slPeerInfoAsyncResponse_t *pEventData = NULL;
            // pEventData = &pSlWlanEvent->EventData.P2PModeDevFound;
            //
        }
        break;

        case SL_WLAN_P2P_NEG_REQ_RECEIVED_EVENT:
        {
            SET_STATUS_BIT(g_ulStatus, STATUS_BIT_P2P_REQ_RECEIVED);
            memset(g_p2p_dev, '\0', MAXIMAL_SSID_LENGTH + 1);
            memcpy(g_p2p_dev,pSlWlanEvent->EventData.P2PModeNegReqReceived.go_peer_device_name,
                    pSlWlanEvent->EventData.P2PModeNegReqReceived.go_peer_device_name_len);
            
            //
            // Information about P2P Negotiation req details (like Peer device
            // name, own SSID etc) will be available in
            // 'slPeerInfoAsyncResponse_t' - Applications can use it if required
            //
            // slPeerInfoAsyncResponse_t *pEventData = NULL;
            // pEventData = &pSlWlanEvent->EventData.P2PModeNegReqReceived;
            //
        }
        break;

        case SL_WLAN_CONNECTION_FAILED_EVENT:
        {
            // If device gets any connection failed event
            SET_STATUS_BIT(g_ulStatus, STATUS_BIT_CONNECTION_FAILED);
        }
        break;
        
        default:
        {
            UART_PRINT("[WLAN EVENT] Unexpected event \n\r");
        }
        break;

    }
}

//*****************************************************************************
//
//! \brief This function handles network events such as IP acquisition, IP
//!           leased, IP released etc.
//!
//! \param[in]  pNetAppEvent - Pointer to NetApp Event Info 
//!
//! \return None
//!
//*****************************************************************************
void SimpleLinkNetAppEventHandler(SlNetAppEvent_t *pNetAppEvent)
{
    SlIpV4AcquiredAsync_t *pEventData = NULL;
    switch(pNetAppEvent->Event)
    {
        case SL_NETAPP_IPV4_IPACQUIRED_EVENT:
            UART_PRINT("SL_NETAPP_IPV4_IPACQUIRED_EVENT\r\n");
            SET_STATUS_BIT(g_ulStatus, STATUS_BIT_IP_AQUIRED);

            pEventData = &pNetAppEvent->EventData.ipAcquiredV4;

            //Ip Acquired Event Data
            g_ulDeviceIp = pNetAppEvent->EventData.ipAcquiredV4.ip;

            //Gateway IP address
            g_ulGatewayIP = pEventData->gateway;
            
            UART_PRINT("[NETAPP EVENT] IP Acquired: IP=%d.%d.%d.%d , "
                        "Gateway=%d.%d.%d.%d\n\r",
            SL_IPV4_BYTE(g_ulDeviceIp,3),
            SL_IPV4_BYTE(g_ulDeviceIp,2),
            SL_IPV4_BYTE(g_ulDeviceIp,1),
            SL_IPV4_BYTE(g_ulDeviceIp,0),
            SL_IPV4_BYTE(g_ulGatewayIP,3),
            SL_IPV4_BYTE(g_ulGatewayIP,2),
            SL_IPV4_BYTE(g_ulGatewayIP,1),
            SL_IPV4_BYTE(g_ulGatewayIP,0));
            
            break;

        case SL_NETAPP_IPV6_IPACQUIRED_EVENT:
        {
            //UART_PRINT("SL_NETAPP_IPV6_IPACQUIRED_EVENT\r\n");
            SET_STATUS_BIT(g_ulStatus, STATUS_BIT_IP_AQUIRED);

            //
            // Information about the IPv4 & IPv6 details (like IP, gateway,dns
            // etc) will be available in 'SlIpV4AcquiredAsync_t / SlIpV6AcquiredAsync_t' - Applications
            // can use it if required
            //
            // For IPv4:
            //
            // SlIpV4AcquiredAsync_t *pEventData = NULL;
            // pEventData = &pNetAppEvent->EventData.ipAcquiredV4;
            //
            // For IPv6:
            //
            // SlIpV6AcquiredAsync_t *pEventData = NULL;
            // pEventData = &pNetAppEvent->EventData.ipAcquiredV6;
            //
            
            //Ip Acquired Event Data
            g_ulDeviceIp = pNetAppEvent->EventData.ipAcquiredV4.ip;

            //Gateway IP address
            g_ulGatewayIP = pEventData->gateway;
            
            UART_PRINT("[NETAPP EVENT] IP Acquired: IP=%d.%d.%d.%d , "
                        "Gateway=%d.%d.%d.%d\n\r",
            SL_IPV4_BYTE(g_ulDeviceIp,3),
            SL_IPV4_BYTE(g_ulDeviceIp,2),
            SL_IPV4_BYTE(g_ulDeviceIp,1),
            SL_IPV4_BYTE(g_ulDeviceIp,0),
            SL_IPV4_BYTE(g_ulGatewayIP,3),
            SL_IPV4_BYTE(g_ulGatewayIP,2),
            SL_IPV4_BYTE(g_ulGatewayIP,1),
            SL_IPV4_BYTE(g_ulGatewayIP,0));
            
        }
        break;

        case SL_NETAPP_IP_LEASED_EVENT:
        {
            SET_STATUS_BIT(g_ulStatus, STATUS_BIT_IP_LEASED);

            //
            // Information about the IP-Leased details (like IP-Leased,
            // lease-time, mac etc) will be available in 'SlIpLeasedAsync_t' -
            // Applications can use it if required
            //
            // SlIpLeasedAsync_t *pEventData = NULL;
            // pEventData = &pNetAppEvent->EventData.ipLeased;
            //

        }
        break;

        default:
        {
            UART_PRINT("[NETAPP EVENT] Unexpected event \n\r");
        }
        break;
    }
}


//*****************************************************************************
//
//! \brief This function handles HTTP server events
//!
//! \param[in]  pServerEvent - Contains the relevant event information
//! \param[in]    pServerResponse - Should be filled by the user with the
//!                                      relevant response information
//!
//! \return None
//!
//****************************************************************************
void SimpleLinkHttpServerCallback(SlHttpServerEvent_t *pHttpEvent,
                                  SlHttpServerResponse_t *pHttpResponse)
{
    // Unused in this application
}

//*****************************************************************************
//
//! \brief This function handles General Events
//!
//! \param[in]     pDevEvent - Pointer to General Event Info 
//!
//! \return None
//!
//*****************************************************************************
void SimpleLinkGeneralEventHandler(SlDeviceEvent_t *pDevEvent)
{
    //
    // Most of the general errors are not FATAL are are to be handled
    // appropriately by the application
    //
    UART_PRINT("[GENERAL EVENT] - ID=[%d] Sender=[%d]\n\n",
               pDevEvent->EventData.deviceEvent.status, 
               pDevEvent->EventData.deviceEvent.sender);
}


//*****************************************************************************
//
//! This function handles socket events indication
//!
//! \param[in]      pSock - Pointer to Socket Event Info
//!
//! \return None
//!
//*****************************************************************************
void SimpleLinkSockEventHandler(SlSockEvent_t *pSock)
{
}

//*****************************************************************************
//
//! \brief This function handles ping report events
//!
//! \param[in]     pPingReport - Ping report statistics
//!
//! \return None
//
//****************************************************************************
void SimpleLinkPingReport(SlPingReport_t *pPingReport)
{
    SET_STATUS_BIT(g_ulStatus, STATUS_BIT_PING_DONE);
    g_ulPingPacketsRecv = pPingReport->PacketsReceived;
}


//*****************************************************************************
// SimpleLink Asynchronous Event Handlers -- End
//*****************************************************************************


//****************************************************************************
//
//!    \brief This function initializes the application variables
//!
//!    \param[in]  None
//!
//!    \return     None
//
//****************************************************************************
static void InitializeAppVariables()
{
    g_ulStatus = 0;
    g_ulStaIp = 0;
    g_ulPingPacketsRecv = 0;
    g_ulGatewayIP = 0;
}

//*****************************************************************************
//! \brief This function puts the device in its default state. It:
//!           - Set the mode to STATION
//!           - Configures connection policy to Auto and AutoSmartConfig
//!           - Deletes all the stored profiles
//!           - Enables DHCP
//!           - Disables Scan policy
//!           - Sets Tx power to maximum
//!           - Sets power policy to normal
//!           - Unregister mDNS services
//!           - Remove all filters
//!
//! \param   none
//! \return  On success, zero is returned. On error, negative is returned
//*****************************************************************************
static long ConfigureSimpleLinkToDefaultState()
{
    SlVersionFull   ver = {0};
    _WlanRxFilterOperationCommandBuff_t  RxFilterIdMask = {0};

    unsigned char ucVal = 1;
    unsigned char ucConfigOpt = 0;
    unsigned char ucConfigLen = 0;
    unsigned char ucPower = 0;

    long lRetVal = -1;
    long lMode = -1;

    lMode = sl_Start(0, 0, 0);
    ASSERT_ON_ERROR(lMode);

    // If the device is not in station-mode, try configuring it in station-mode 
    if (ROLE_STA != lMode)
    {
        if (ROLE_AP == lMode)
        {
            // If the device is in AP mode, we need to wait for this event 
            // before doing anything 
            while(!IS_IP_ACQUIRED(g_ulStatus))
            {
#ifndef SL_PLATFORM_MULTI_THREADED
              _SlNonOsMainLoopTask(); 
#endif
            }
        }

        // Switch to STA role and restart 
        lRetVal = sl_WlanSetMode(ROLE_STA);
        ASSERT_ON_ERROR(lRetVal);

        lRetVal = sl_Stop(0xFF);
        ASSERT_ON_ERROR(lRetVal);

        lRetVal = sl_Start(0, 0, 0);
        ASSERT_ON_ERROR(lRetVal);

        // Check if the device is in station again 
        if (ROLE_STA != lRetVal)
        {
            // We don't want to proceed if the device is not coming up in STA-mode 
            return DEVICE_NOT_IN_STATION_MODE;
        }
    }
    
    // Get the device's version-information
    ucConfigOpt = SL_DEVICE_GENERAL_VERSION;
    ucConfigLen = sizeof(ver);
    lRetVal = sl_DevGet(SL_DEVICE_GENERAL_CONFIGURATION, &ucConfigOpt, 
                                &ucConfigLen, (unsigned char *)(&ver));
    ASSERT_ON_ERROR(lRetVal);
    
    UART_PRINT("Host Driver Version: %s\n\r",SL_DRIVER_VERSION);
    UART_PRINT("Build Version %d.%d.%d.%d.31.%d.%d.%d.%d.%d.%d.%d.%d\n\r",
    ver.NwpVersion[0],ver.NwpVersion[1],ver.NwpVersion[2],ver.NwpVersion[3],
    ver.ChipFwAndPhyVersion.FwVersion[0],ver.ChipFwAndPhyVersion.FwVersion[1],
    ver.ChipFwAndPhyVersion.FwVersion[2],ver.ChipFwAndPhyVersion.FwVersion[3],
    ver.ChipFwAndPhyVersion.PhyVersion[0],ver.ChipFwAndPhyVersion.PhyVersion[1],
    ver.ChipFwAndPhyVersion.PhyVersion[2],ver.ChipFwAndPhyVersion.PhyVersion[3]);

    // Set connection policy to Auto + SmartConfig 
    //      (Device's default connection policy)
    lRetVal = sl_WlanPolicySet(SL_POLICY_CONNECTION, 
                                SL_CONNECTION_POLICY(1, 0, 0, 0, 1), NULL, 0);
    ASSERT_ON_ERROR(lRetVal);

    // Remove all profiles
    lRetVal = sl_WlanProfileDel(0xFF);
    ASSERT_ON_ERROR(lRetVal);

    //
    // Device in station-mode. Disconnect previous connection if any
    // The function returns 0 if 'Disconnected done', negative number if already
    // disconnected Wait for 'disconnection' event if 0 is returned, Ignore 
    // other return-codes
    //
    lRetVal = sl_WlanDisconnect();
    if(0 == lRetVal)
    {
        // Wait
        while(IS_CONNECTED(g_ulStatus))
        {
#ifndef SL_PLATFORM_MULTI_THREADED
              _SlNonOsMainLoopTask(); 
#endif
        }
    }

    // Enable DHCP client
    lRetVal = sl_NetCfgSet(SL_IPV4_STA_P2P_CL_DHCP_ENABLE,1,1,&ucVal);
    ASSERT_ON_ERROR(lRetVal);

    // Disable scan
    ucConfigOpt = SL_SCAN_POLICY(0);
    lRetVal = sl_WlanPolicySet(SL_POLICY_SCAN , ucConfigOpt, NULL, 0);
    ASSERT_ON_ERROR(lRetVal);

    // Set Tx power level for station mode
    // Number between 0-15, as dB offset from max power - 0 will set max power
    ucPower = 0;
    lRetVal = sl_WlanSet(SL_WLAN_CFG_GENERAL_PARAM_ID, 
            WLAN_GENERAL_PARAM_OPT_STA_TX_POWER, 1, (unsigned char *)&ucPower);
    ASSERT_ON_ERROR(lRetVal);

    // Set PM policy to normal
    lRetVal = sl_WlanPolicySet(SL_POLICY_PM , SL_NORMAL_POLICY, NULL, 0);
    ASSERT_ON_ERROR(lRetVal);

    // Unregister mDNS services
    lRetVal = sl_NetAppMDNSUnRegisterService(0, 0);
    ASSERT_ON_ERROR(lRetVal);

    // Remove  all 64 filters (8*8)
    memset(RxFilterIdMask.FilterIdMask, 0xFF, 8);
    lRetVal = sl_WlanRxFilterSet(SL_REMOVE_RX_FILTER, (_u8 *)&RxFilterIdMask,
                       sizeof(_WlanRxFilterOperationCommandBuff_t));
    ASSERT_ON_ERROR(lRetVal);

    lRetVal = sl_Stop(SL_STOP_TIMEOUT);
    ASSERT_ON_ERROR(lRetVal);

    InitializeAppVariables();
    
    return lRetVal; // Success
}

static long WlanConnect()
{
    SlSecParams_t secParams = {0};
    long lRetVal = 0;

    secParams.Key = (signed char *)P2P_SECURITY_KEY;
    secParams.KeyLen = strlen(P2P_SECURITY_KEY);
    secParams.Type = P2P_SECURITY_TYPE;

    lRetVal = sl_WlanConnect((signed char *)P2P_REMOTE_DEVICE,
                            strlen((const char *)P2P_REMOTE_DEVICE), 0,
                            &secParams, 0);
    if (lRetVal)
    {
        UART_PRINT("WlanConnect failed\n\r");
        LOOP_FOREVER();
    }

    // Wait till Device acquired an IP in P2P mode
    while(! IS_P2P_REQ_RCVD(g_ulStatus))
    { 
        //_SlNonOsMainLoopTask();
    }

    // Connect with the device requesting the connection
    lRetVal = sl_WlanConnect((signed char *)g_p2p_dev,
                           strlen((const char *)g_p2p_dev),
                           0, &secParams, 0);
    ASSERT_ON_ERROR(lRetVal);
    
#ifdef P2P_ROLE_TYPE_NEGOTIATE
    while(! IS_IP_ACQUIRED(g_ulStatus))
#else
    while(! IS_IP_LEASED(g_ulStatus))
#endif
    {
        //_SlNonOsMainLoopTask();
        if(IS_CONNECT_FAILED(g_ulStatus))
        {
            // Error, connection is failed
            ASSERT_ON_ERROR(NETWORK_CONNECTION_FAILED);
        }
    }

    return SUCCESS;
}

static signed long DisplayIP()
{
    unsigned long mask, dns, gateway;
    long lRetVal = -1;
    unsigned char len, dhcpIsOn;

    if(g_ulDeviceIp == 0)
    {
        len = sizeof(SlNetCfgIpV4Args_t);
        dhcpIsOn = 0;
        SlNetCfgIpV4Args_t ipV4 = {0};

#ifdef P2P_ROLE_TYPE_NEGOTIATE
        lRetVal = sl_NetCfgGet(SL_IPV4_AP_P2P_GO_GET_INFO,&dhcpIsOn,
                                        &len,(unsigned char *)&ipV4);
#else
        lRetVal = sl_NetCfgGet(SL_IPV4_STA_P2P_CL_GET_INFO,&dhcpIsOn,
                                                &len,(unsigned char *)&ipV4)
#endif
        ASSERT_ON_ERROR(lRetVal);

        g_ulDeviceIp=ipV4.ipV4;
        mask=ipV4.ipV4Mask;
        gateway=ipV4.ipV4Gateway;
        dns=ipV4.ipV4DnsServer;
    }

    UNUSED(mask);
    UNUSED(gateway);
    UNUSED(dns);

    UART_PRINT("CC3200 Device IP : IP=%d.%d.%d.%d \n\r",
                SL_IPV4_BYTE(g_ulDeviceIp,3), SL_IPV4_BYTE(g_ulDeviceIp,2),
                SL_IPV4_BYTE(g_ulDeviceIp,1), SL_IPV4_BYTE(g_ulDeviceIp,0));
    return 0;
}

//****************************************************************************
//
//! Confgiures the mode in which the device will work
//!
//! \param iMode is the current mode of the device
//!
//! This function
//!    1. prompt user for desired configuration and accordingly configure the
//!          networking mode(STA or AP).
//!       2. also give the user the option to configure the ssid name in case of
//!       AP mode.
//!
//! \return sl_start return value(int).
//
//****************************************************************************
static int ConfigureMode(int iMode)
{
    char    pcSsidName[33] = "my_cc3200_wl";
    long   lRetVal = -1;

    UART_PRINT("Enter the AP SSID name: %s\r\n", pcSsidName);
    //GetSsidName(pcSsidName,33);

    lRetVal = sl_WlanSetMode(iMode);
    ASSERT_ON_ERROR(lRetVal);

    lRetVal = sl_WlanSet(SL_WLAN_CFG_AP_ID, WLAN_AP_OPT_SSID, strlen(pcSsidName),
                            (unsigned char*)pcSsidName);
    ASSERT_ON_ERROR(lRetVal);

    UART_PRINT("Device is configured in AP mode\n\r");

    /* Restart Network processor */
    lRetVal = sl_Stop(SL_STOP_TIMEOUT);

    // reset status bits
    CLR_STATUS_BIT_ALL(g_ulStatus);

    return sl_Start(NULL,NULL,NULL);
}

//****************************************************************************
//
//! Get Ssid name form the user over UART
//!
//! \param pcSsidName is a pointer to the array which will contain the ssid name
//!
//! This function
//!    1. gets the ssid name string over uart
//!
//! \return iRetVal is the length of the ssid(user input).
//
//****************************************************************************
static int GetSsidName(char *pcSsidName, unsigned int uiMaxLen)
{
  char ucRecvdAPDetails = 0;
  int  iRetVal = 0;
  char acCmdStore[128];
  do
  {
      ucRecvdAPDetails = 0;

      //
      // Get the AP name to connect over the UART
      //
      iRetVal = GetCmd(acCmdStore, sizeof(acCmdStore));
      if(iRetVal > 0)
      {
          // remove start/end spaces if any
          iRetVal = TrimSpace(acCmdStore);

          //
          // Parse the AP name
          //
          strncpy(pcSsidName, acCmdStore, iRetVal);
          if(pcSsidName != NULL)
          {
              ucRecvdAPDetails = 1;
              pcSsidName[iRetVal] = '\0';
          }
      }
  }while(ucRecvdAPDetails == 0);

  return(iRetVal);
}

//****************************************************************************
//
//!    \brief device will try to ping the machine that has just connected to the
//!           device.
//!
//!    \param  ulIpAddr is the ip address of the station which has connected to
//!            device
//!
//!    \return 0 if ping is successful, -1 for error
//
//****************************************************************************
static int PingTest(unsigned long ulIpAddr)
{  
    signed long           lRetVal = -1;
    SlPingStartCommand_t PingParams;
    SlPingReport_t PingReport;
    PingParams.PingIntervalTime = PING_INTERVAL;
    PingParams.PingSize = PING_PKT_SIZE;
    PingParams.PingRequestTimeout = PING_TIMEOUT;
    PingParams.TotalNumberOfAttempts = NO_OF_ATTEMPTS;
    PingParams.Flags = PING_FLAG;
    PingParams.Ip = ulIpAddr; /* Cleint's ip address */
    
    UART_PRINT("Running Ping Test...\n\r");
    /* Check for LAN connection */
    lRetVal = sl_NetAppPingStart((SlPingStartCommand_t*)&PingParams, SL_AF_INET,
                            (SlPingReport_t*)&PingReport, NULL);
    ASSERT_ON_ERROR(lRetVal);

    g_ulPingPacketsRecv = PingReport.PacketsReceived;

    if (g_ulPingPacketsRecv > 0 && g_ulPingPacketsRecv <= NO_OF_ATTEMPTS)
    {
      // LAN connection is successful
      UART_PRINT("Ping Test successful\n\r");
    }
    else
    {
        // Problem with LAN connection
        ASSERT_ON_ERROR(NETWORK_CONNECTION_FAILED);
    }

    return SUCCESS;
}

long P2PConfiguration()
{
    unsigned char ucP2PParam[4];
    long lRetVal = -1;

    // Set any p2p option (SL_CONNECTION_POLICY(0,0,0,any_p2p,0)) to connect to
    // first available p2p device
    lRetVal = sl_WlanPolicySet(SL_POLICY_CONNECTION,SL_CONNECTION_POLICY(1,0,0,0,0),NULL,0);
    ASSERT_ON_ERROR(lRetVal);

    // Set the negotiation role (SL_P2P_ROLE_NEGOTIATE).
    // CC3200 will negotiate with remote device GO/client mode.
    // Other valid options are:
    //             - SL_P2P_ROLE_GROUP_OWNER
    //             - SL_P2P_ROLE_CLIENT
    //             - SL_P2P_ROLE_NEGOTIATE
    lRetVal = sl_WlanPolicySet(SL_POLICY_P2P, SL_P2P_POLICY(SL_P2P_ROLE_CLIENT,
                                   SL_P2P_NEG_INITIATOR_ACTIVE),NULL,0);
    ASSERT_ON_ERROR(lRetVal);

    // Set P2P Device name
    lRetVal = sl_NetAppSet(SL_NET_APP_DEVICE_CONFIG_ID, \
                             NETAPP_SET_GET_DEV_CONF_OPT_DEVICE_URN,
                           strlen(P2P_DEVICE_NAME), (unsigned char *)P2P_DEVICE_NAME);
    ASSERT_ON_ERROR(lRetVal);

    // Set P2P device type
    lRetVal = sl_WlanSet(SL_WLAN_CFG_P2P_PARAM_ID, WLAN_P2P_OPT_DEV_TYPE,
                         strlen(P2P_CONFIG_VALUE), (unsigned char*)P2P_CONFIG_VALUE);
    ASSERT_ON_ERROR(lRetVal);

    // setting P2P channel parameters
    ucP2PParam[0] = LISENING_CHANNEL;
    ucP2PParam[1] = REGULATORY_CLASS;
    ucP2PParam[2] = OPERATING_CHANNEL;
    ucP2PParam[3] = REGULATORY_CLASS;

    // Set P2P Device listen and open channel valid channels are 1/6/11
    lRetVal = sl_WlanSet(SL_WLAN_CFG_P2P_PARAM_ID, WLAN_P2P_OPT_CHANNEL_N_REGS,
                sizeof(ucP2PParam), ucP2PParam);
    ASSERT_ON_ERROR(lRetVal);

    // Restart as P2P device
    lRetVal = sl_Stop(SL_STOP_TIMEOUT);

    lRetVal = sl_Start(NULL,NULL,NULL);
    if(lRetVal < 0 || lRetVal != ROLE_P2P)
    {
        ASSERT_ON_ERROR(P2P_CONFIG_FAILED);
    }
    else
    {
        UART_PRINT("Connect to %s \n\r",P2P_DEVICE_NAME);
    }

    return SUCCESS;
}

//****************************************************************************
//
//!    \brief start simplelink, wait for the sta to connect to the device and 
//!        run the ping test for that sta
//!
//!    \param  pvparameters is the pointer to the list of parameters that can be
//!         passed to the task while creating it
//!
//!    \return None
//
//****************************************************************************
void WlanAPMode( void *pvParameters )
{   
    while (1) {
    int iTestResult = 0;
    unsigned char ucDHCP;
    long lRetVal = -1;

    InitializeAppVariables();

    //
    // Following function configure the device to default state by cleaning
    // the persistent settings stored in NVMEM (viz. connection profiles &
    // policies, power policy etc)
    //
    // Applications may choose to skip this step if the developer is sure
    // that the device is in its default state at start of applicaton
    //
    // Note that all profiles and persistent settings that were done on the
    // device will be lost
    //
    lRetVal = ConfigureSimpleLinkToDefaultState();
    if(lRetVal < 0)
    {
        if (P2P_MODE_START_FAILED == lRetVal)
            UART_PRINT("Failed to configure the device in its default state \n\r");

        LOOP_FOREVER();
    }

    UART_PRINT("Device is configured in default state \n\r");

    //
    // Asumption is that the device is configured in station mode already
    // and it is in its default state
    //
    lRetVal = sl_Start(NULL,NULL,NULL);

    if (lRetVal < 0)
    {
        UART_PRINT("Failed to start the device \n\r");
        LOOP_FOREVER();
    }

    // UART_PRINT("Device started as STATION \n\r");
    
    //
    // Configure the networking mode and ssid name(for AP mode)
    //
    if(lRetVal != ROLE_P2P)
    {
        lRetVal = sl_WlanSetMode(ROLE_P2P);
        ASSERT_ON_ERROR(lRetVal);

        lRetVal = sl_Stop(0xFF);

        // reset the Status bits
        CLR_STATUS_BIT_ALL(g_ulStatus);

        lRetVal = sl_Start(NULL,NULL,NULL);
        if(lRetVal < 0 || lRetVal != ROLE_P2P)
        {
            ASSERT_ON_ERROR(P2P_CONFIG_FAILED);
        }
        else
        {
            UART_PRINT("Started SimpleLink Device: P2P Mode\n\r");
        }
    }

    if(lRetVal < 0)
    {
        UART_PRINT("Setting P2P configuration failed\n\r");
        LOOP_FOREVER();
    }

    lRetVal = P2PConfiguration();
    if(lRetVal < 0)
    {
        UART_PRINT("Setting P2P configuration failed\n\r");
        LOOP_FOREVER();
    }

    UART_PRINT("WlanConnect\r\n");
    lRetVal = WlanConnect();

    if(lRetVal == 0)
    {
    }
    else
    {
        UART_PRINT("Reset the device and try again\n\r");
        LOOP_FOREVER();
    }

    if(DisplayIP() < 0)
    {
        UART_PRINT("Get IP address failed \n\r");
        LOOP_FOREVER();
    }

    if(!(IS_CONNECT_FAILED(g_ulStatus)))
    {
        UART_PRINT("Starting TCP server or client\r\n");
        //lRetVal = BsdTcpServer(PORT_NUM);
        lRetVal = SomeTcpClient(PORT_NUM);
        if(lRetVal >= 0)
        {
            UART_PRINT("Received TCP packets successfully \n\r");
        }
        else if(lRetVal == CLIENT_DISCONNECTED)
        {
            UART_PRINT("P2P Client disconnected \n\r");
        }
        else
        {
            UART_PRINT("TCP packets receive failed \n\r");
        }

    }

    if(lRetVal >=0)
    {
        UART_PRINT("Test passed, exiting application... \n\r");
    }
    else
    {
        UART_PRINT("Test failed, exiting application... \n\r");
    }

    // revert Device into STA mode and power off Network Processor
    lRetVal = sl_WlanSetMode(ROLE_STA);
    if(lRetVal < 0)
    {
         ERR_PRINT(lRetVal);
         LOOP_FOREVER();
    }

    lRetVal = sl_Stop(SL_STOP_TIMEOUT);
    }
}

//*****************************************************************************
//
//! Application startup display on UART
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
static void
DisplayBanner(char * AppName)
{
    Report("\n\n\n\r");
    Report("\t\t *************************************************\n\r");
    Report("\t\t       CC3200 %s Application       \n\r", AppName);
    Report("\t\t *************************************************\n\r");
    Report("\n\n\n\r");
}

//*****************************************************************************
//
//! Board Initialization & Configuration
//!
//! \param  None
//!
//! \return None
//
//*****************************************************************************
static void
BoardInit(void)
{
/* In case of TI-RTOS vector table is initialize by OS itself */
#ifndef USE_TIRTOS
    //
    // Set vector table base
    //
#if defined(ccs) || defined(gcc)
    MAP_IntVTableBaseSet((unsigned long)&g_pfnVectors[0]);
#endif
#if defined(ewarm)
    MAP_IntVTableBaseSet((unsigned long)&__vector_table);
#endif
#endif
    //
    // Enable Processor
    //
    MAP_IntMasterEnable();
    MAP_IntEnable(FAULT_SYSTICK);

    PRCMCC3200MCUInit();
}


//*****************************************************************************
//                            MAIN FUNCTION
//*****************************************************************************
void main()
{
    long lRetVal = -1;
  
    //
    // Board Initialization
    //
    BoardInit();
    
    //
    // Configure the pinmux settings for the peripherals exercised
    //
    PinMuxConfig();
    
#ifndef NOTERM
    //
    // Configuring UART
    //
    InitTerm();
#endif

    //
    // Display banner
    //
    DisplayBanner(APP_NAME);
    
    //
    // Start the SimpleLink Host
    //
    lRetVal = VStartSimpleLinkSpawnTask(SPAWN_TASK_PRIORITY);
    if(lRetVal < 0)
    {
        ERR_PRINT(lRetVal);
        LOOP_FOREVER();
    }
    
    //
    // Start the WlanAPMode task
    //
    lRetVal = osi_TaskCreate( WlanAPMode, \
                            (const signed char*)"wireless LAN in AP mode", \
                            OSI_STACK_SIZE, NULL, 1, NULL );
    if(lRetVal < 0)
    {
        ERR_PRINT(lRetVal);
        LOOP_FOREVER();
    }
    
    //
    // Start the task scheduler
    //
    osi_start();
}

//*****************************************************************************
//
// Close the Doxygen group.
//! @}
//
//*****************************************************************************
