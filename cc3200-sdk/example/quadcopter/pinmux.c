#include "pinmux.h" 
#include "hw_types.h"
#include "hw_memmap.h"
#include "hw_gpio.h"
#include "pin.h"
#include "gpio.h"
#include "prcm.h"

//*****************************************************************************
void PinMuxConfig(void)
{
    //
    // Enable Peripheral Clocks 
    //
    PRCMPeripheralClkEnable(PRCM_UARTA0, PRCM_RUN_MODE_CLK);
    PRCMPeripheralClkEnable(PRCM_I2CA0, PRCM_RUN_MODE_CLK);
    PRCMPeripheralClkEnable(PRCM_TIMERA3, PRCM_RUN_MODE_CLK);
    PRCMPeripheralClkEnable(PRCM_TIMERA1, PRCM_RUN_MODE_CLK);
    PRCMPeripheralClkEnable(PRCM_TIMERA2, PRCM_RUN_MODE_CLK);
    PRCMPeripheralClkEnable(PRCM_CAMERA, PRCM_RUN_MODE_CLK);

    //
    // Configure PIN_52 for UART0 UART0_RTS
    //
    PinTypeUART(PIN_52, PIN_MODE_6);

    //
    // Configure PIN_50 for UART0 UART0_CTS
    //
    PinTypeUART(PIN_50, PIN_MODE_12);

    //
    // Configure PIN_62 for UART0 UART0_TX
    //
    PinTypeUART(PIN_62, PIN_MODE_11);

    //
    // Configure PIN_45 for UART0 UART0_RX
    //
    PinTypeUART(PIN_45, PIN_MODE_9);

    //
    // Configure PIN_16 for I2C0 I2C_SCL
    //
    PinTypeI2C(PIN_16, PIN_MODE_9);

    //
    // Configure PIN_17 for I2C0 I2C_SDA
    //
    PinTypeI2C(PIN_17, PIN_MODE_9);

    //
    // Configure PIN_63 for TimerCP6 GT_CCP06
    //
    PinTypeTimer(PIN_63, PIN_MODE_12);

    //
    // Configure PIN_57 for TimerCP2 GT_CCP02
    //
    PinTypeTimer(PIN_57, PIN_MODE_7);

    //
    // Configure PIN_15 for TimerCP4 GT_CCP04
    //
    PinTypeTimer(PIN_15, PIN_MODE_5);

    //
    // Configure PIN_53 for TimerCP5 GT_CCP05
    //
    PinTypeTimer(PIN_53, PIN_MODE_4);

    //
    // Configure PIN_55 for Camera0 PIXCLK
    //
    PinTypeCamera(PIN_55, PIN_MODE_4);

    //
    // Configure PIN_58 for Camera0 CAM_pDATA7
    //
    PinTypeCamera(PIN_58, PIN_MODE_4);

    //
    // Configure PIN_59 for Camera0 CAM_pDATA6
    //
    PinTypeCamera(PIN_59, PIN_MODE_4);

    //
    // Configure PIN_60 for Camera0 CAM_pDATA5
    //
    PinTypeCamera(PIN_60, PIN_MODE_4);

    //
    // Configure PIN_61 for Camera0 CAM_pDATA4
    //
    PinTypeCamera(PIN_61, PIN_MODE_4);

    //
    // Configure PIN_02 for Camera0 pXCLK
    //
    PinTypeCamera(PIN_02, PIN_MODE_4);

    //
    // Configure PIN_03 for Camera0 VSYNC
    //
    PinTypeCamera(PIN_03, PIN_MODE_4);

    //
    // Configure PIN_04 for Camera0 HSYNC
    //
    PinTypeCamera(PIN_04, PIN_MODE_4);

    //
    // Configure PIN_05 for Camera0 CAM_pDATA8
    //
    PinTypeCamera(PIN_05, PIN_MODE_4);
    
    //
    // Configure PIN_06 for Camera0 CAM_pDATA9
    //
    PinTypeCamera(PIN_06, PIN_MODE_4);

    //
    // Configure PIN_07 for Camera0 CAM_pDATA10
    //
    PinTypeCamera(PIN_07, PIN_MODE_4);

    //
    // Configure PIN_08 for Camera0 CAM_pDATA11
    //
    PinTypeCamera(PIN_08, PIN_MODE_4);

    //
    // Configure PIN_01 for TimerPWM6 GT_PWM06
    //
    PinTypeTimer(PIN_01, PIN_MODE_3);

    //
    // Configure PIN_64 for TimerPWM5 GT_PWM05
    //
    PinTypeTimer(PIN_64, PIN_MODE_3);

    //
    // Configure PIN_21 for TimerPWM2 GT_PWM02
    //
    PinTypeTimer(PIN_21, PIN_MODE_9);

    //
    // Configure PIN_19 for TimerPWM3 GT_PWM03
    //
    PinTypeTimer(PIN_19, PIN_MODE_8);
}