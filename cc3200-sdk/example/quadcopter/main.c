#include <stdlib.h>
#include <string.h>

#include "rom.h"
#include "rom_map.h"
#include "hw_memmap.h"
#include "hw_common_reg.h"
#include "hw_types.h"
#include "hw_ints.h"
#include "uart.h"
#include "interrupt.h"
#include "pinmux.h"
#include "utils.h"
#include "prcm.h"
#include "simplelink.h"

#include "uart_if.h"
#include "udma_if.h"
#include "gpio_if.h"
#include "common.h"
#include "camera_app.h"
#include "osi.h"

#define OSI_STACK_SIZE 1024
#define FIRMWARE_VERSION "0.105"

volatile unsigned char g_CaptureImage = 0;
OsiTaskHandle g_CameraTaskHandle;

extern void (* const g_pfnVectors[])(void);
void CameraService(void *pvParameters);
void local_InitTerm(void);

void
vAssertCalled( const char *pcFile, unsigned long ulLine )
{
    while(1)
    {
    }
}

void
vApplicationIdleHook(void)
{
}

/* this hook gets called when FreeRTOS fails to malloc */
void
vApplicationMallocFailedHook()
{
    while(1)
    {
    }
}

void
vApplicationStackOverflowHook(OsiTaskHandle *pxTask, signed char *pcTaskName)
{
    while(1)
    {
    }
}

void
CameraService(void *pvParameters)
{
    UART_PRINT("Mock Camera Service Started\r\n");
    //StartCamera();
    osi_TaskDelete(&g_CameraTaskHandle);
}

void SimpleLinkHttpServerCallback(SlHttpServerEvent_t *pSlHttpServerEvent, 
                               SlHttpServerResponse_t *pSlHttpServerResponse)
{
    if((pSlHttpServerEvent == NULL) || (pSlHttpServerResponse == NULL))
    {
        UART_PRINT("Null pointer\n\r");
        LOOP_FOREVER();
    }

    switch (pSlHttpServerEvent->Event)
    {
       case SL_NETAPP_HTTPPOSTTOKENVALUE_EVENT:
        {
              if (0 == memcmp (pSlHttpServerEvent->
                       EventData.httpPostData.token_name.data, "__SL_P_U.C", 
                    pSlHttpServerEvent->EventData.httpPostData.token_name.len))
              {
                  if(0 == memcmp (pSlHttpServerEvent->EventData.httpPostData.token_value.data, \
                                                    "start", \
                                     pSlHttpServerEvent->EventData.httpPostData.token_value.len))
                  {
                      g_CaptureImage = 1;
                  }
                  else
                  {
                      g_CaptureImage = 0;
                  }
              }
        }
          break;
        default:
          break;
    }
}

static void
BoardInit(void)
{
    MAP_IntVTableBaseSet((unsigned long)&g_pfnVectors[0]);

    MAP_IntMasterEnable();
    MAP_IntEnable(FAULT_SYSTICK);

    PRCMCC3200MCUInit();
}

void
local_InitTerm(void)
{
  MAP_UARTConfigSetExpClk(UARTA0_BASE,MAP_PRCMPeripheralClockGet(PRCM_UARTA0), 
                  115200, (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                   UART_CONFIG_PAR_NONE));
}

void
main()
{
    long lRetVal = -1;

    BoardInit();

    PinMuxConfig();

    //InitTerm();
    local_InitTerm();

    ClearTerm();

    Report("QuadCopter Firmware Testing\r\n");
    Report("Firmware version %s\r\n", FIRMWARE_VERSION);
    
    lRetVal = VStartSimpleLinkSpawnTask (SPAWN_TASK_PRIORITY);
    if(lRetVal < 0)
    {
        ERR_PRINT(lRetVal);
        LOOP_FOREVER();
    }

    lRetVal = osi_TaskCreate(CameraService,
        (signed char *) "CameraService",
        OSI_STACK_SIZE, NULL, 1, &g_CameraTaskHandle);

    if(lRetVal < 0)
    {
        ERR_PRINT(lRetVal);
        LOOP_FOREVER();
    }

    osi_start();

    Report("code shouldn't reach here\r\n");

    while(1){};
}
