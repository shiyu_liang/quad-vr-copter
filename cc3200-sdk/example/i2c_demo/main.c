//*****************************************************************************
//
//! \addtogroup i2c_demo
//! @{
//
//*****************************************************************************

// Standard includes
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdlib.h>

// Driverlib includes
#include "hw_types.h"
#include "uart.h"

// Common interface includes
#include "uart_if.h"
#include "i2c_if.h"

#include "pinmux.h"
#include "hw_i2c.h"
#include "hw_ints.h"
#include "hw_common_reg.h"
#include "hw_memmap.h" // has *_BASE defines
#include "hw_gpio.h"
#include "hw_camera.h"
#include "debug.h"
#include "utils.h"
#include "i2c.h"
#include "hw_i2c.h"
#include "rom.h"
#include "rom_map.h"
#include "gpio.h"
#include "interrupt.h"
#include "prcm.h"
#include "pin.h"
#include "timer.h"
#include "camera.h"
#include "ov7670.h"

//*****************************************************************************
//                      MACRO DEFINITIONS
//*****************************************************************************
#define APPLICATION_VERSION     "1.1.0"
#define APP_NAME                "I2C Demo"
#define UART_PRINT              Report
#define FOREVER                 1
#define CONSOLE                 UARTA0_BASE
#define FAILURE                 -1
#define SUCCESS                 0
#define RETERR_IF_TRUE(condition) {if(condition) return FAILURE;}
#define RET_IF_ERR(Func)          {int iRetVal = (Func); \
                                   if (SUCCESS != iRetVal) \
                                     return  iRetVal;}

#define TIMER_INTERVAL_RELOAD   6
#define CAM_BT_CORRECT_EN   0x00001000      
#define I2C_BASE                I2CA0_BASE

#define OV7670_I2C_ADDR 0x21

//*****************************************************************************
//                 GLOBAL VARIABLES -- Start
//*****************************************************************************
#if defined(ccs)
extern void (* const g_pfnVectors[])(void);
#endif
#if defined(ewarm)
extern uVectorEntry __vector_table;
#endif

static unsigned char g_frame_end;

//*****************************************************************************
//                 GLOBAL VARIABLES -- End
//*****************************************************************************

uint8_t ov7670_R[] = {
    0x0A, // PID
    0x0B, // VER
    0x1C, // MIDH
    0x1D  // MIDL
};

uint8_t i2c_regread (unsigned char slave_addr, unsigned char start_reg){
    unsigned char rec;

    MAP_I2CMasterSlaveAddrSet(I2C_BASE, slave_addr, false); // ready to send
    MAP_I2CMasterDataPut(I2C_BASE, start_reg);
    MAP_I2CMasterIntClearEx(I2C_BASE,MAP_I2CMasterIntStatusEx(I2C_BASE,false));
    MAP_I2CMasterControl(I2C_BASE, I2C_MASTER_CMD_SINGLE_SEND);
    
    while((MAP_I2CMasterIntStatusEx(I2C_BASE, false) 
                & (I2C_INT_MASTER | I2C_MRIS_CLKTOUT)) == 0)
    {}
    
    

    if(MAP_I2CMasterErr(I2C_BASE) != I2C_MASTER_ERR_NONE)
        UART_PRINT("ERROR: I2C read error 1");

    MAP_I2CMasterSlaveAddrSet(I2C_BASE, slave_addr, true); // ready to receive
MAP_I2CMasterIntClearEx(I2C_BASE,MAP_I2CMasterIntStatusEx(I2C_BASE,false));
    MAP_I2CMasterControl(I2C_BASE, I2C_MASTER_CMD_SINGLE_RECEIVE);

    
    while((MAP_I2CMasterIntStatusEx(I2C_BASE, false) 
                & (I2C_INT_MASTER | I2C_MRIS_CLKTOUT)) == 0)
    {
    }
    

    if(MAP_I2CMasterErr(I2C_BASE) != I2C_MASTER_ERR_NONE)
        UART_PRINT("ERROR: I2C read error 2");

    while (MAP_I2CMasterBusy(I2C_BASE)) {};

    rec = MAP_I2CMasterDataGet(I2C_BASE);    
}

void i2c_regwrite (uint8_t slave_addr, uint8_t reg, uint8_t data) {
    MAP_I2CMasterSlaveAddrSet(I2C_BASE, slave_addr, false);
    MAP_I2CMasterDataPut(I2C_BASE, reg);

    MAP_I2CMasterIntClearEx(I2C_BASE,MAP_I2CMasterIntStatusEx(I2C_BASE,false));
    MAP_I2CMasterControl(I2C_BASE, I2C_MASTER_CMD_BURST_SEND_START);
    
    while((MAP_I2CMasterIntStatusEx(I2C_BASE, false) 
                & (I2C_INT_MASTER | I2C_MRIS_CLKTOUT)) == 0)
    {}
    
    

    if(MAP_I2CMasterErr(I2C_BASE) != I2C_MASTER_ERR_NONE)
        UART_PRINT("ERROR: I2C write error 1");

    MAP_I2CMasterDataPut(I2C_BASE, data);
    MAP_I2CMasterIntClearEx(I2C_BASE,MAP_I2CMasterIntStatusEx(I2C_BASE,false));
    MAP_I2CMasterControl(I2C_BASE, I2C_MASTER_CMD_BURST_SEND_CONT);
    
    while((MAP_I2CMasterIntStatusEx(I2C_BASE, false) 
                & (I2C_INT_MASTER | I2C_MRIS_CLKTOUT)) == 0)
    {}
    
    

    if(MAP_I2CMasterErr(I2C_BASE) != I2C_MASTER_ERR_NONE)
        UART_PRINT("ERROR: I2C write error 2");

    MAP_I2CMasterIntClearEx(I2C_BASE,MAP_I2CMasterIntStatusEx(I2C_BASE,false));
    MAP_I2CMasterControl(I2C_BASE, I2C_MASTER_CMD_BURST_SEND_STOP);
    
    while((MAP_I2CMasterIntStatusEx(I2C_BASE, false) 
                & (I2C_INT_MASTER | I2C_MRIS_CLKTOUT)) == 0)
    {}
    
    

    if(MAP_I2CMasterErr(I2C_BASE) != I2C_MASTER_ERR_NONE)
        UART_PRINT("ERROR: I2C write error 3");
}

//*****************************************************************************
//
//! Board Initialization & Configuration
//!
//! \param  None
//!
//! \return None
//
//*****************************************************************************
static void
BoardInit(void)
{
/* In case of TI-RTOS vector table is initialize by OS itself */
#ifndef USE_TIRTOS
    //
    // Set vector table base
    //
#if defined(ccs)
    MAP_IntVTableBaseSet((unsigned long)&g_pfnVectors[0]);
#endif
#if defined(ewarm)
    MAP_IntVTableBaseSet((unsigned long)&__vector_table);
#endif
#endif
    //
    // Enable Processor
    //
    MAP_IntMasterEnable();
    MAP_IntEnable(FAULT_SYSTICK);

    PRCMCC3200MCUInit();
}

static void CameraIntHandler()
{
}

//*****************************************************************************
//
//! Main function handling the I2C example
//!
//! \param  None
//!
//! \return None
//! 
//*****************************************************************************
void main()
{
    int iRetVal;
    long lRetVal;
    char acCmdStore[512];
	unsigned char slave_addr;
	unsigned char dummy_buff;
    
    //
    // Initialize board configurations
    //
    BoardInit();
    
    //
    // Configure the pinmux settings for the peripherals exercised
    //
    PinMuxConfig();
    
    //
    // Configuring UART
    //
    InitTerm();
    
    //
    // I2C Init
    //
    
    MAP_PRCMPeripheralClkEnable(PRCM_I2CA0, PRCM_RUN_MODE_CLK);
    MAP_PRCMPeripheralReset(PRCM_I2CA0);
    MAP_I2CMasterInitExpClk(I2CA0_BASE,80000000,false);
    

    //
    // PWM Init
    //
    
    MAP_TimerConfigure(TIMERA2_BASE,(TIMER_CFG_SPLIT_PAIR | TIMER_CFG_B_PWM));
    MAP_TimerPrescaleSet(TIMERA2_BASE,TIMER_B, 0);
    MAP_TimerControlLevel(TIMERA2_BASE,TIMER_B, true);
    MAP_TimerLoadSet(TIMERA2_BASE,TIMER_B,TIMER_INTERVAL_RELOAD);
    MAP_TimerMatchSet(TIMERA2_BASE,TIMER_B,TIMER_INTERVAL_RELOAD/2);
    MAP_TimerEnable(TIMERA2_BASE,TIMER_B);    
    

    // Camera Init
    Report("Initializing Camera Controller\r\n");
    
    MAP_PRCMPeripheralClkEnable(PRCM_CAMERA, PRCM_RUN_MODE_CLK);
    MAP_PRCMPeripheralReset(PRCM_CAMERA);

#ifndef ENABLE_JPEG
    // Configure Camera clock from ARCM
    // CamClkIn = ((240)/((1+1)+(1+1))) = 60 MHz
    HWREG(0x44025000) = 0x0101;
#else
    HWREG(0x44025000) = 0x0000;
#endif

    MAP_CameraReset(CAMERA_BASE);

#ifndef ENABLE_JPEG
    MAP_CameraParamsConfig(CAMERA_BASE, CAM_HS_POL_HI, CAM_VS_POL_HI,
                       CAM_PCLK_RISE_EDGE|CAM_ORDERCAM_SWAP|CAM_NOBT_SYNCHRO);
#else
    MAP_CameraParamsConfig(CAMERA_BASE, CAM_HS_POL_HI,CAM_VS_POL_HI,
                       CAM_PCLK_RISE_EDGE|CAM_NOBT_SYNCHRO|CAM_IF_SYNCHRO|CAM_BT_CORRECT_EN);
#endif

    MAP_CameraIntRegister(CAMERA_BASE, CameraIntHandler);

#ifndef ENABLE_JPEG
    MAP_CameraXClkConfig(CAMERA_BASE, 60000000,3750000);
#else
    MAP_CameraXClkConfig(CAMERA_BASE, 120000000,16000000);
#endif

    MAP_CameraThresholdSet(CAMERA_BASE, 8);
    MAP_CameraIntEnable(CAMERA_BASE, CAM_INT_FE);
    MAP_CameraDMAEnable(CAMERA_BASE);
    
    Report("Initializing Camera Sensor\r\n");

    //
    // Display the banner followed by the usage description
    //
    //DisplayBanner(APP_NAME);
    //DisplayUsage();

    UART_PRINT("\r\n");
    UART_PRINT("--- Remove jumpers J10 and J11 ---\n\r");
    //UART_PRINT("    Press SW2 to start testing    \n\r");
    // while (!GPIOPinRead(GPIOA2_BASE, 0x40)) {};
    
    slave_addr = 0x21;
    unsigned char start_reg = 0x00;
    unsigned char end_reg = 0xc9;
    unsigned char reg_vals[0xca];
    unsigned char rec;
    unsigned char addr;
    uint8_t id_buff[4];
    uint8_t cnt = sizeof (ov7670_R) / sizeof (ov7670_R[0]);
    
    I2C_IF_Open(I2C_MASTER_MODE_STD); // FST mode seems to work as well
    
    for (addr = 0; addr < cnt ; addr++)
        id_buff[addr] = i2c_regread (slave_addr, ov7670_R[addr]);

    UART_PRINT("[INFO] OV%2x%2x\r\n", id_buff[0], id_buff[1]);
    UART_PRINT("[INFO] Manufacturer %2x%2x\r\n", id_buff[2], id_buff[3]);
    
    UART_PRINT("[TEST] Test writing to registers\r\n");
    rec = i2c_regread (slave_addr, 0x00);
    UART_PRINT("[DUMP] Read reg = 0x%x with value 0x%x\r\n", 0x00, rec);
    i2c_regwrite(slave_addr, 0x00, 0x5a);
    rec = i2c_regread (slave_addr, 0x00);
    UART_PRINT("[DUMP] Read reg = 0x%x with value 0x%x\r\n", 0x00, rec);

    i2c_regwrite (OV7670_I2C_ADDR, 0x12, 0x80); // reset all registers to default values
    // i2c_regwrite (OV7670_I2C_ADDR, 0x11, 0x80); // set internal clock to XCLK
    MAP_UtilsDelay(80000);

    /* set output format to YUV/YCbCr 4:2:2 */
    rec = i2c_regread (OV7670_I2C_ADDR, REG_COM7);
    i2c_regwrite (OV7670_I2C_ADDR, REG_COM7, rec &= 0xFA);
    /* MSB and LSB not swapped */
    rec = i2c_regread (OV7670_I2C_ADDR, REG_COM3);
    i2c_regwrite (OV7670_I2C_ADDR, REG_COM7, rec &= 0xBF);
    /* set internal clock to XCLK */
    i2c_regwrite (OV7670_I2C_ADDR, REG_CLKRC, 0x80);
    /* set HREF, PCLK and VSYNC behaviours to default */
    i2c_regwrite (OV7670_I2C_ADDR, REG_COM10, 0x00);
    /* scale PCLK to 1MHz */
    i2c_regwrite (OV7670_I2C_ADDR, REG_COM14, 0x1c); // set PCLK divider to 16
    i2c_regwrite (OV7670_I2C_ADDR, 0x73, 0x04);

    /* image capture infinite loop */
    while (true)
    {
        MAP_CameraCaptureStart (CAMERA_BASE);
        while (g_frame_end = 0);
        MAP_CameraCaptureStop (CAMERA_BASE, true);
    }

	return;
}

//*****************************************************************************
//
// Close the Doxygen group.
//! @
//
//*****************************************************************************


