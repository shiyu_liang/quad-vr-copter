EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:bl-ctrl_1.2-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "11 jan 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ATMEGA8-AI IC1
U 1 1 54B0A5F7
P 3050 2500
F 0 "IC1" H 2300 3700 40  0000 L BNN
F 1 "ATMEGA8-AI" H 3550 950 40  0000 L BNN
F 2 "TQFP32" H 3050 2500 30  0000 C CIN
F 3 "" H 3050 2500 60  0000 C CNN
	1    3050 2500
	1    0    0    -1  
$EndComp
Text Label 4050 1500 0    60   ~ 0
PWM
Text Label 4050 1600 0    60   ~ 0
C+
Text Label 4050 1700 0    60   ~ 0
B+
Text Label 4050 1800 0    60   ~ 0
A+
Text Label 4050 1900 0    60   ~ 0
SDA
Text Label 4050 2000 0    60   ~ 0
SCL
Text Label 4050 2200 0    60   ~ 0
NULL_A
Text Label 4050 2300 0    60   ~ 0
NULL_B
Text Label 4050 2400 0    60   ~ 0
NULL_C
Text Label 4050 2500 0    60   ~ 0
LED_RED
Text Label 4050 2600 0    60   ~ 0
SDA
Text Label 4050 2700 0    60   ~ 0
SCL
Text Label 4050 2900 0    60   ~ 0
V_BAT
Text Label 2150 2200 2    60   ~ 0
ADDR1
Text Label 2150 2400 2    60   ~ 0
ADDR2
Text Label 4050 3100 0    60   ~ 0
RXD
Text Label 4050 3200 0    60   ~ 0
TXD
Text Label 4050 3300 0    60   ~ 0
INT0
Text Label 4050 3400 0    60   ~ 0
A-
Text Label 4050 3500 0    60   ~ 0
B-
Text Label 4050 3600 0    60   ~ 0
C-
Text Label 4050 3700 0    60   ~ 0
NULL
Text Label 4050 3800 0    60   ~ 0
LED_GRN
$Comp
L GND #PWR01
U 1 1 54B0A707
P 3050 4150
F 0 "#PWR01" H 3050 4150 30  0001 C CNN
F 1 "GND" H 3050 4080 30  0001 C CNN
F 2 "" H 3050 4150 60  0000 C CNN
F 3 "" H 3050 4150 60  0000 C CNN
	1    3050 4150
	1    0    0    -1  
$EndComp
Text Label 1450 1500 2    60   ~ 0
RESET
$Comp
L R R3
U 1 1 54B0A7C7
P 1950 1200
F 0 "R3" V 2030 1200 40  0000 C CNN
F 1 "10k" V 1957 1201 40  0000 C CNN
F 2 "" V 1880 1200 30  0000 C CNN
F 3 "" H 1950 1200 30  0000 C CNN
	1    1950 1200
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR02
U 1 1 54B0A7FA
P 1950 900
F 0 "#PWR02" H 1950 990 20  0001 C CNN
F 1 "+5V" H 1950 990 30  0000 C CNN
F 2 "" H 1950 900 60  0000 C CNN
F 3 "" H 1950 900 60  0000 C CNN
	1    1950 900 
	1    0    0    -1  
$EndComp
$Comp
L C C7
U 1 1 54B0A820
P 1600 1750
F 0 "C7" H 1600 1850 40  0000 L CNN
F 1 "0.1uF" H 1606 1665 40  0000 L CNN
F 2 "" H 1638 1600 30  0000 C CNN
F 3 "" H 1600 1750 60  0000 C CNN
	1    1600 1750
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 54B0A848
P 1600 2000
F 0 "#PWR03" H 1600 2000 30  0001 C CNN
F 1 "GND" H 1600 1930 30  0001 C CNN
F 2 "" H 1600 2000 60  0000 C CNN
F 3 "" H 1600 2000 60  0000 C CNN
	1    1600 2000
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 54B0A8CC
P 1150 1950
F 0 "R1" V 1230 1950 40  0000 C CNN
F 1 "10k" V 1157 1951 40  0000 C CNN
F 2 "" V 1080 1950 30  0000 C CNN
F 3 "" H 1150 1950 30  0000 C CNN
	1    1150 1950
	1    0    0    -1  
$EndComp
$Comp
L C C4
U 1 1 54B0A908
P 1150 2450
F 0 "C4" H 1150 2550 40  0000 L CNN
F 1 "0.1uF" H 1156 2365 40  0000 L CNN
F 2 "" H 1188 2300 30  0000 C CNN
F 3 "" H 1150 2450 60  0000 C CNN
	1    1150 2450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 54B0A933
P 1150 2700
F 0 "#PWR04" H 1150 2700 30  0001 C CNN
F 1 "GND" H 1150 2630 30  0001 C CNN
F 2 "" H 1150 2700 60  0000 C CNN
F 3 "" H 1150 2700 60  0000 C CNN
	1    1150 2700
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR05
U 1 1 54B0A943
P 1150 1650
F 0 "#PWR05" H 1150 1740 20  0001 C CNN
F 1 "+5V" H 1150 1740 30  0000 C CNN
F 2 "" H 1150 1650 60  0000 C CNN
F 3 "" H 1150 1650 60  0000 C CNN
	1    1150 1650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR06
U 1 1 54B0A9A2
P 2100 1950
F 0 "#PWR06" H 2100 1950 30  0001 C CNN
F 1 "GND" H 2100 1880 30  0001 C CNN
F 2 "" H 2100 1950 60  0000 C CNN
F 3 "" H 2100 1950 60  0000 C CNN
	1    2100 1950
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR07
U 1 1 54B0AAB3
P 3050 1100
F 0 "#PWR07" H 3050 1190 20  0001 C CNN
F 1 "+5V" H 3050 1190 30  0000 C CNN
F 2 "" H 3050 1100 60  0000 C CNN
F 3 "" H 3050 1100 60  0000 C CNN
	1    3050 1100
	1    0    0    -1  
$EndComp
$Comp
L R R10
U 1 1 54B0AEFB
P 5200 2800
F 0 "R10" V 5280 2800 40  0000 C CNN
F 1 "680" V 5207 2801 40  0000 C CNN
F 2 "" V 5130 2800 30  0000 C CNN
F 3 "" H 5200 2800 30  0000 C CNN
	1    5200 2800
	0    1    1    0   
$EndComp
$Comp
L C C17
U 1 1 54B0AF6E
P 4950 3050
F 0 "C17" H 4950 3150 40  0000 L CNN
F 1 "0.1uF" H 4956 2965 40  0000 L CNN
F 2 "" H 4988 2900 30  0000 C CNN
F 3 "" H 4950 3050 60  0000 C CNN
	1    4950 3050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR08
U 1 1 54B0AFA1
P 4950 3300
F 0 "#PWR08" H 4950 3300 30  0001 C CNN
F 1 "GND" H 4950 3230 30  0001 C CNN
F 2 "" H 4950 3300 60  0000 C CNN
F 3 "" H 4950 3300 60  0000 C CNN
	1    4950 3300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR09
U 1 1 54B0AFC5
P 5450 2900
F 0 "#PWR09" H 5450 2900 30  0001 C CNN
F 1 "GND" H 5450 2830 30  0001 C CNN
F 2 "" H 5450 2900 60  0000 C CNN
F 3 "" H 5450 2900 60  0000 C CNN
	1    5450 2900
	1    0    0    -1  
$EndComp
$Comp
L R R16
U 1 1 54B0B0F6
P 7000 1900
F 0 "R16" V 7080 1900 40  0000 C CNN
F 1 "18k" V 7007 1901 40  0000 C CNN
F 2 "" V 6930 1900 30  0000 C CNN
F 3 "" H 7000 1900 30  0000 C CNN
	1    7000 1900
	1    0    0    -1  
$EndComp
$Comp
L R R17
U 1 1 54B0B1A1
P 7000 2500
F 0 "R17" V 7080 2500 40  0000 C CNN
F 1 "1k" V 7007 2501 40  0000 C CNN
F 2 "" V 6930 2500 30  0000 C CNN
F 3 "" H 7000 2500 30  0000 C CNN
	1    7000 2500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR010
U 1 1 54B0B1E1
P 7000 2800
F 0 "#PWR010" H 7000 2800 30  0001 C CNN
F 1 "GND" H 7000 2730 30  0001 C CNN
F 2 "" H 7000 2800 60  0000 C CNN
F 3 "" H 7000 2800 60  0000 C CNN
	1    7000 2800
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR011
U 1 1 54B0B267
P 7000 1550
F 0 "#PWR011" H 7000 1650 30  0001 C CNN
F 1 "VCC" H 7000 1650 30  0000 C CNN
F 2 "" H 7000 1550 60  0000 C CNN
F 3 "" H 7000 1550 60  0000 C CNN
	1    7000 1550
	1    0    0    -1  
$EndComp
$Comp
L C C21
U 1 1 54B0B27B
P 6800 2450
F 0 "C21" H 6800 2550 40  0000 L CNN
F 1 "0.1uF" H 6806 2365 40  0000 L CNN
F 2 "" H 6838 2300 30  0000 C CNN
F 3 "" H 6800 2450 60  0000 C CNN
	1    6800 2450
	1    0    0    -1  
$EndComp
Text Label 6400 2250 2    60   ~ 0
V_BAT
$Comp
L LED D2
U 1 1 54B0BAAC
P 4750 3800
F 0 "D2" H 4750 3900 50  0000 C CNN
F 1 "LED" H 4750 3700 50  0000 C CNN
F 2 "" H 4750 3800 60  0000 C CNN
F 3 "" H 4750 3800 60  0000 C CNN
	1    4750 3800
	-1   0    0    1   
$EndComp
$Comp
L +5V #PWR012
U 1 1 54B0BAF3
P 5600 3800
F 0 "#PWR012" H 5600 3890 20  0001 C CNN
F 1 "+5V" H 5600 3890 30  0000 C CNN
F 2 "" H 5600 3800 60  0000 C CNN
F 3 "" H 5600 3800 60  0000 C CNN
	1    5600 3800
	1    0    0    -1  
$EndComp
$Comp
L R R12
U 1 1 54B0BB07
P 5250 3800
F 0 "R12" V 5330 3800 40  0000 C CNN
F 1 "1k" V 5257 3801 40  0000 C CNN
F 2 "" V 5180 3800 30  0000 C CNN
F 3 "" H 5250 3800 30  0000 C CNN
	1    5250 3800
	0    1    1    0   
$EndComp
$Comp
L LED D1
U 1 1 54B0BD47
P 4750 2500
F 0 "D1" H 4750 2600 50  0000 C CNN
F 1 "LED" H 4750 2400 50  0000 C CNN
F 2 "" H 4750 2500 60  0000 C CNN
F 3 "" H 4750 2500 60  0000 C CNN
	1    4750 2500
	1    0    0    -1  
$EndComp
$Comp
L R R11
U 1 1 54B0BD84
P 5250 2500
F 0 "R11" V 5330 2500 40  0000 C CNN
F 1 "1k" V 5257 2501 40  0000 C CNN
F 2 "" V 5180 2500 30  0000 C CNN
F 3 "" H 5250 2500 30  0000 C CNN
	1    5250 2500
	0    1    1    0   
$EndComp
$Comp
L GND #PWR013
U 1 1 54B0BDBE
P 5550 2550
F 0 "#PWR013" H 5550 2550 30  0001 C CNN
F 1 "GND" H 5550 2480 30  0001 C CNN
F 2 "" H 5550 2550 60  0000 C CNN
F 3 "" H 5550 2550 60  0000 C CNN
	1    5550 2550
	1    0    0    -1  
$EndComp
$Comp
L uA78L05 U1
U 1 1 54B0C0A2
P 2600 4650
F 0 "U1" H 2600 4450 60  0000 C CNN
F 1 "uA78L05" H 2600 4750 60  0000 C CNN
F 2 "" H 2600 4650 60  0000 C CNN
F 3 "" H 2600 4650 60  0000 C CNN
	1    2600 4650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR014
U 1 1 54B0C152
P 2200 4800
F 0 "#PWR014" H 2200 4800 30  0001 C CNN
F 1 "GND" H 2200 4730 30  0001 C CNN
F 2 "" H 2200 4800 60  0000 C CNN
F 3 "" H 2200 4800 60  0000 C CNN
	1    2200 4800
	1    0    0    -1  
$EndComp
$Comp
L CP2 C3
U 1 1 54B0C1AB
P 1100 5500
F 0 "C3" H 1100 5600 40  0000 L CNN
F 1 "330uF" H 1106 5415 40  0000 L CNN
F 2 "" H 1138 5350 30  0000 C CNN
F 3 "" H 1100 5500 60  0000 C CNN
	1    1100 5500
	1    0    0    -1  
$EndComp
$Comp
L CP2 C16
U 1 1 54B0C1FC
P 3900 4850
F 0 "C16" H 3900 4950 40  0000 L CNN
F 1 "10uF" H 3906 4765 40  0000 L CNN
F 2 "" H 3938 4700 30  0000 C CNN
F 3 "" H 3900 4850 60  0000 C CNN
	1    3900 4850
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 54B0C2BF
P 700 5500
F 0 "C1" H 700 5600 40  0000 L CNN
F 1 "0.1uF" H 706 5415 40  0000 L CNN
F 2 "0603" H 738 5350 30  0000 C CNN
F 3 "" H 700 5500 60  0000 C CNN
	1    700  5500
	1    0    0    -1  
$EndComp
$Comp
L C C2
U 1 1 54B0C34C
P 900 5500
F 0 "C2" H 900 5600 40  0000 L CNN
F 1 "0.1uF" H 906 5415 40  0000 L CNN
F 2 "0603" H 938 5350 30  0000 C CNN
F 3 "" H 900 5500 60  0000 C CNN
	1    900  5500
	1    0    0    -1  
$EndComp
$Comp
L C C5
U 1 1 54B0C401
P 1300 5500
F 0 "C5" H 1300 5600 40  0000 L CNN
F 1 "0.1uF" H 1306 5415 40  0000 L CNN
F 2 "" H 1338 5350 30  0000 C CNN
F 3 "" H 1300 5500 60  0000 C CNN
	1    1300 5500
	1    0    0    -1  
$EndComp
$Comp
L C C6
U 1 1 54B0C43B
P 1500 5500
F 0 "C6" H 1500 5600 40  0000 L CNN
F 1 "0.1uF" H 1506 5415 40  0000 L CNN
F 2 "" H 1538 5350 30  0000 C CNN
F 3 "" H 1500 5500 60  0000 C CNN
	1    1500 5500
	1    0    0    -1  
$EndComp
$Comp
L C C8
U 1 1 54B0C472
P 1700 5500
F 0 "C8" H 1700 5600 40  0000 L CNN
F 1 "0.1uF" H 1706 5415 40  0000 L CNN
F 2 "" H 1738 5350 30  0000 C CNN
F 3 "" H 1700 5500 60  0000 C CNN
	1    1700 5500
	1    0    0    -1  
$EndComp
$Comp
L C C9
U 1 1 54B0C4A4
P 1900 5500
F 0 "C9" H 1900 5600 40  0000 L CNN
F 1 "0.1uF" H 1906 5415 40  0000 L CNN
F 2 "" H 1938 5350 30  0000 C CNN
F 3 "" H 1900 5500 60  0000 C CNN
	1    1900 5500
	1    0    0    -1  
$EndComp
$Comp
L C C10
U 1 1 54B0C4D5
P 2100 5500
F 0 "C10" H 2100 5600 40  0000 L CNN
F 1 "0.1uF" H 2106 5415 40  0000 L CNN
F 2 "" H 2138 5350 30  0000 C CNN
F 3 "" H 2100 5500 60  0000 C CNN
	1    2100 5500
	1    0    0    -1  
$EndComp
$Comp
L C C11
U 1 1 54B0C5AA
P 2300 5500
F 0 "C11" H 2300 5600 40  0000 L CNN
F 1 "0.1uF" H 2306 5415 40  0000 L CNN
F 2 "" H 2338 5350 30  0000 C CNN
F 3 "" H 2300 5500 60  0000 C CNN
	1    2300 5500
	1    0    0    -1  
$EndComp
$Comp
L C C12
U 1 1 54B0C5DD
P 2500 5500
F 0 "C12" H 2500 5600 40  0000 L CNN
F 1 "0.1uF" H 2506 5415 40  0000 L CNN
F 2 "" H 2538 5350 30  0000 C CNN
F 3 "" H 2500 5500 60  0000 C CNN
	1    2500 5500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR015
U 1 1 54B0C5F7
P 1600 5750
F 0 "#PWR015" H 1600 5750 30  0001 C CNN
F 1 "GND" H 1600 5680 30  0001 C CNN
F 2 "" H 1600 5750 60  0000 C CNN
F 3 "" H 1600 5750 60  0000 C CNN
	1    1600 5750
	1    0    0    -1  
$EndComp
$Comp
L C C13
U 1 1 54B0C9E2
P 3300 4850
F 0 "C13" H 3300 4950 40  0000 L CNN
F 1 "0.1uF" H 3306 4765 40  0000 L CNN
F 2 "" H 3338 4700 30  0000 C CNN
F 3 "" H 3300 4850 60  0000 C CNN
	1    3300 4850
	1    0    0    -1  
$EndComp
$Comp
L C C14
U 1 1 54B0CB32
P 3500 4850
F 0 "C14" H 3500 4950 40  0000 L CNN
F 1 "0.1uF" H 3506 4765 40  0000 L CNN
F 2 "" H 3538 4700 30  0000 C CNN
F 3 "" H 3500 4850 60  0000 C CNN
	1    3500 4850
	1    0    0    -1  
$EndComp
$Comp
L C C15
U 1 1 54B0CBD0
P 3700 4850
F 0 "C15" H 3700 4950 40  0000 L CNN
F 1 "0.1uF" H 3706 4765 40  0000 L CNN
F 2 "" H 3738 4700 30  0000 C CNN
F 3 "" H 3700 4850 60  0000 C CNN
	1    3700 4850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR016
U 1 1 54B0CD1F
P 3600 5100
F 0 "#PWR016" H 3600 5100 30  0001 C CNN
F 1 "GND" H 3600 5030 30  0001 C CNN
F 2 "" H 3600 5100 60  0000 C CNN
F 3 "" H 3600 5100 60  0000 C CNN
	1    3600 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 4100 3100 4100
Wire Wire Line
	3050 4150 3050 4100
Connection ~ 3050 4100
Wire Wire Line
	2150 1500 1450 1500
Wire Wire Line
	1950 1450 1950 1500
Connection ~ 1950 1500
Wire Wire Line
	1600 1550 1600 1500
Connection ~ 1600 1500
Wire Wire Line
	1600 1950 1600 2000
Wire Wire Line
	1150 2200 1150 2250
Wire Wire Line
	1850 1700 1850 2050
Wire Wire Line
	1850 2050 1700 2050
Wire Wire Line
	1700 2050 1700 2200
Wire Wire Line
	1700 2200 1150 2200
Wire Wire Line
	2100 1800 2100 1950
Wire Wire Line
	2100 1900 2150 1900
Wire Wire Line
	3000 1200 3000 1150
Wire Wire Line
	3000 1150 3100 1150
Wire Wire Line
	3050 1150 3050 1100
Wire Wire Line
	3100 1150 3100 1200
Connection ~ 3050 1150
Wire Wire Line
	2150 1800 2100 1800
Connection ~ 2100 1900
Wire Wire Line
	4050 2800 4950 2800
Wire Wire Line
	4950 2800 4950 2850
Wire Wire Line
	4950 3300 4950 3250
Wire Wire Line
	5450 2800 5450 2900
Wire Wire Line
	6400 2250 7000 2250
Wire Wire Line
	7000 2250 7000 2150
Wire Wire Line
	7000 1650 7000 1550
Wire Wire Line
	7000 2750 7000 2800
Wire Wire Line
	6800 2650 6800 2750
Wire Wire Line
	6800 2750 7000 2750
Connection ~ 6800 2250
Wire Wire Line
	4050 3800 4550 3800
Wire Wire Line
	4950 3800 5000 3800
Wire Wire Line
	5500 3800 5600 3800
Wire Wire Line
	4050 2500 4550 2500
Wire Wire Line
	4950 2500 5000 2500
Wire Wire Line
	5500 2500 5550 2500
Wire Wire Line
	5550 2500 5550 2550
Wire Wire Line
	2200 4750 2200 4800
Wire Wire Line
	700  5700 2500 5700
Connection ~ 900  5700
Connection ~ 1100 5700
Connection ~ 1300 5700
Connection ~ 1500 5700
Wire Wire Line
	1600 5750 1600 5700
Connection ~ 1600 5700
Connection ~ 1700 5700
Connection ~ 1900 5700
Connection ~ 2100 5700
Connection ~ 2300 5700
Wire Wire Line
	3300 5050 3900 5050
Connection ~ 3500 5050
Connection ~ 3700 5050
Wire Wire Line
	3600 5100 3600 5050
Connection ~ 3600 5050
Wire Wire Line
	3000 4650 3900 4650
Connection ~ 3300 4650
Connection ~ 3500 4650
Connection ~ 3700 4650
Wire Wire Line
	700  5300 2500 5300
Connection ~ 900  5300
Connection ~ 1100 5300
Connection ~ 1300 5300
Connection ~ 1500 5300
Connection ~ 1700 5300
Connection ~ 1900 5300
Connection ~ 2100 5300
Connection ~ 2300 5300
Wire Wire Line
	1900 5300 1900 4650
Wire Wire Line
	1900 4650 2200 4650
$Comp
L GS3 GS1
U 1 1 54B0D8A8
P 1650 2450
F 0 "GS1" H 1700 2650 50  0000 C CNN
F 1 "GS3" H 1700 2251 40  0000 C CNN
F 2 "GS3" V 1738 2376 30  0000 C CNN
F 3 "" H 1650 2450 60  0000 C CNN
	1    1650 2450
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR017
U 1 1 54B0D8D3
P 1450 2500
F 0 "#PWR017" H 1450 2500 30  0001 C CNN
F 1 "GND" H 1450 2430 30  0001 C CNN
F 2 "" H 1450 2500 60  0000 C CNN
F 3 "" H 1450 2500 60  0000 C CNN
	1    1450 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 2450 1450 2450
Wire Wire Line
	1450 2450 1450 2500
Wire Wire Line
	1800 2350 1800 2200
Wire Wire Line
	1800 2200 2150 2200
Wire Wire Line
	2150 2400 1800 2400
Wire Wire Line
	1800 2400 1800 2550
$Comp
L CONN_01X01 P1
U 1 1 54B0DCD7
P 1300 5050
F 0 "P1" H 1300 5150 50  0000 C CNN
F 1 "CONN_01X01" V 1400 5050 50  0000 C CNN
F 2 "" H 1300 5050 60  0000 C CNN
F 3 "" H 1300 5050 60  0000 C CNN
	1    1300 5050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1300 5250 1300 5300
$Comp
L HE10-10 P2
U 1 1 54B0DFD3
P 2500 6750
F 0 "P2" H 2500 7300 70  0000 C CNN
F 1 "HE10-10" H 2500 6150 70  0000 C CNN
F 2 "" H 2500 6750 60  0000 C CNN
F 3 "" H 2500 6750 60  0000 C CNN
	1    2500 6750
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 54B0E04C
P 1550 6350
F 0 "R2" V 1630 6350 40  0000 C CNN
F 1 "4.7k" V 1557 6351 40  0000 C CNN
F 2 "" V 1480 6350 30  0000 C CNN
F 3 "" H 1550 6350 30  0000 C CNN
	1    1550 6350
	0    1    1    0   
$EndComp
Text Label 1300 6350 2    60   ~ 0
A+
Text Label 1950 6350 2    60   ~ 0
MOSI
Wire Wire Line
	1950 6350 1800 6350
Text Label 1950 6750 2    60   ~ 0
RESET
Text Label 1950 6950 2    60   ~ 0
SCL
Text Label 1950 7150 2    60   ~ 0
SDA
Text Label 3050 6550 0    60   ~ 0
PWM
Text Label 3050 6750 0    60   ~ 0
INT0
Text Label 3050 6950 0    60   ~ 0
RXD
$Comp
L GND #PWR018
U 1 1 54B0E405
P 3150 7200
F 0 "#PWR018" H 3150 7200 30  0001 C CNN
F 1 "GND" H 3150 7130 30  0001 C CNN
F 2 "" H 3150 7200 60  0000 C CNN
F 3 "" H 3150 7200 60  0000 C CNN
	1    3150 7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 7150 3150 7150
Wire Wire Line
	3150 7150 3150 7200
$Comp
L +5V #PWR019
U 1 1 54B0E4FE
P 3150 6250
F 0 "#PWR019" H 3150 6340 20  0001 C CNN
F 1 "+5V" H 3150 6340 30  0000 C CNN
F 2 "" H 3150 6250 60  0000 C CNN
F 3 "" H 3150 6250 60  0000 C CNN
	1    3150 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 6350 3150 6350
Wire Wire Line
	3150 6350 3150 6250
$Comp
L R R4
U 1 1 54B0E75A
P 4350 5850
F 0 "R4" V 4430 5850 40  0000 C CNN
F 1 "4.7K" V 4357 5851 40  0000 C CNN
F 2 "" V 4280 5850 30  0000 C CNN
F 3 "" H 4350 5850 30  0000 C CNN
	1    4350 5850
	0    1    1    0   
$EndComp
$Comp
L C C18
U 1 1 54B0EDA6
P 5500 6700
F 0 "C18" H 5500 6800 40  0000 L CNN
F 1 "0.1uF" H 5506 6615 40  0000 L CNN
F 2 "" H 5538 6550 30  0000 C CNN
F 3 "" H 5500 6700 60  0000 C CNN
	1    5500 6700
	1    0    0    -1  
$EndComp
$Comp
L C C19
U 1 1 54B0EE25
P 5700 6700
F 0 "C19" H 5700 6800 40  0000 L CNN
F 1 "0.1uF" H 5706 6615 40  0000 L CNN
F 2 "" H 5738 6550 30  0000 C CNN
F 3 "" H 5700 6700 60  0000 C CNN
	1    5700 6700
	1    0    0    -1  
$EndComp
$Comp
L C C20
U 1 1 54B0EE6D
P 5900 6700
F 0 "C20" H 5900 6800 40  0000 L CNN
F 1 "0.1uF" H 5906 6615 40  0000 L CNN
F 2 "" H 5938 6550 30  0000 C CNN
F 3 "" H 5900 6700 60  0000 C CNN
	1    5900 6700
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR020
U 1 1 54B0EE87
P 5350 7100
F 0 "#PWR020" H 5350 7100 30  0001 C CNN
F 1 "GND" H 5350 7030 30  0001 C CNN
F 2 "" H 5350 7100 60  0000 C CNN
F 3 "" H 5350 7100 60  0000 C CNN
	1    5350 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 7000 5500 7000
Connection ~ 4950 7000
Wire Wire Line
	5500 6900 5900 6900
Connection ~ 5700 6900
Wire Wire Line
	5500 7000 5500 6900
Connection ~ 5150 7000
Wire Wire Line
	5350 7000 5350 7100
Connection ~ 5350 7000
Wire Wire Line
	4600 6250 6100 6250
Wire Wire Line
	4600 6050 6100 6050
Wire Wire Line
	6100 5850 4600 5850
Wire Wire Line
	4750 6500 4750 6250
Connection ~ 4750 6250
Wire Wire Line
	4950 6500 4950 6050
Connection ~ 4950 6050
Wire Wire Line
	5150 6500 5150 5850
Connection ~ 5150 5850
Wire Wire Line
	5500 5100 5500 6500
Connection ~ 5500 6250
Wire Wire Line
	5700 5250 5700 6500
Connection ~ 5700 6050
Wire Wire Line
	5900 5400 5900 6500
Connection ~ 5900 5850
$Comp
L R R5
U 1 1 54B0FB9B
P 4350 6050
F 0 "R5" V 4430 6050 40  0000 C CNN
F 1 "4.7K" V 4357 6051 40  0000 C CNN
F 2 "" V 4280 6050 30  0000 C CNN
F 3 "" H 4350 6050 30  0000 C CNN
	1    4350 6050
	0    1    1    0   
$EndComp
$Comp
L R R6
U 1 1 54B0FC05
P 4350 6250
F 0 "R6" V 4430 6250 40  0000 C CNN
F 1 "4.7K" V 4357 6251 40  0000 C CNN
F 2 "" V 4280 6250 30  0000 C CNN
F 3 "" H 4350 6250 30  0000 C CNN
	1    4350 6250
	0    1    1    0   
$EndComp
$Comp
L R R13
U 1 1 54B0FC47
P 6350 5850
F 0 "R13" V 6430 5850 40  0000 C CNN
F 1 "4.7K" V 6357 5851 40  0000 C CNN
F 2 "" V 6280 5850 30  0000 C CNN
F 3 "" H 6350 5850 30  0000 C CNN
	1    6350 5850
	0    1    1    0   
$EndComp
$Comp
L R R14
U 1 1 54B0FCD3
P 6350 6050
F 0 "R14" V 6430 6050 40  0000 C CNN
F 1 "4.7K" V 6357 6051 40  0000 C CNN
F 2 "" V 6280 6050 30  0000 C CNN
F 3 "" H 6350 6050 30  0000 C CNN
	1    6350 6050
	0    1    1    0   
$EndComp
$Comp
L R R15
U 1 1 54B0FD27
P 6350 6250
F 0 "R15" V 6430 6250 40  0000 C CNN
F 1 "4.7K" V 6357 6251 40  0000 C CNN
F 2 "" V 6280 6250 30  0000 C CNN
F 3 "" H 6350 6250 30  0000 C CNN
	1    6350 6250
	0    1    1    0   
$EndComp
$Comp
L R R7
U 1 1 54B0FF51
P 4750 6750
F 0 "R7" V 4830 6750 40  0000 C CNN
F 1 "4.7K" V 4757 6751 40  0000 C CNN
F 2 "" V 4680 6750 30  0000 C CNN
F 3 "" H 4750 6750 30  0000 C CNN
	1    4750 6750
	-1   0    0    1   
$EndComp
$Comp
L R R8
U 1 1 54B0FFA2
P 4950 6750
F 0 "R8" V 5030 6750 40  0000 C CNN
F 1 "4.7K" V 4957 6751 40  0000 C CNN
F 2 "" V 4880 6750 30  0000 C CNN
F 3 "" H 4950 6750 30  0000 C CNN
	1    4950 6750
	-1   0    0    1   
$EndComp
$Comp
L R R9
U 1 1 54B10042
P 5150 6750
F 0 "R9" V 5230 6750 40  0000 C CNN
F 1 "4.7K" V 5157 6751 40  0000 C CNN
F 2 "" V 5080 6750 30  0000 C CNN
F 3 "" H 5150 6750 30  0000 C CNN
	1    5150 6750
	-1   0    0    1   
$EndComp
Text Label 4100 5850 2    60   ~ 0
PHASE_A
Text Label 4100 6050 2    60   ~ 0
PHASE_B
Text Label 4100 6250 2    60   ~ 0
PHASE_C
Wire Wire Line
	6600 5850 6750 5850
Wire Wire Line
	6750 5850 6750 6250
Wire Wire Line
	6750 6250 6600 6250
Wire Wire Line
	6600 6050 6900 6050
Connection ~ 6750 6050
Text Label 6900 6050 0    60   ~ 0
NULL
Text Label 6250 5100 0    60   ~ 0
NULL_C
Text Label 6250 5250 0    60   ~ 0
NULL_B
Text Label 6250 5400 0    60   ~ 0
NULL_A
Wire Wire Line
	6250 5400 5900 5400
Wire Wire Line
	6250 5250 5700 5250
Wire Wire Line
	6250 5100 5500 5100
$Comp
L CONN_01X01 P3
U 1 1 54B115DA
P 3600 7200
F 0 "P3" H 3600 7300 50  0000 C CNN
F 1 "CONN_01X01" V 3700 7200 50  0000 C CNN
F 2 "" H 3600 7200 60  0000 C CNN
F 3 "" H 3600 7200 60  0000 C CNN
	1    3600 7200
	0    -1   -1   0   
$EndComp
$Comp
L CONN_01X01 P4
U 1 1 54B116E4
P 3800 7200
F 0 "P4" H 3800 7300 50  0000 C CNN
F 1 "CONN_01X01" V 3900 7200 50  0000 C CNN
F 2 "" H 3800 7200 60  0000 C CNN
F 3 "" H 3800 7200 60  0000 C CNN
	1    3800 7200
	0    -1   -1   0   
$EndComp
$Comp
L CONN_01X01 P5
U 1 1 54B11737
P 4000 7200
F 0 "P5" H 4000 7300 50  0000 C CNN
F 1 "CONN_01X01" V 4100 7200 50  0000 C CNN
F 2 "" H 4000 7200 60  0000 C CNN
F 3 "" H 4000 7200 60  0000 C CNN
	1    4000 7200
	0    -1   -1   0   
$EndComp
$Comp
L CONN_01X01 P6
U 1 1 54B11783
P 4200 7200
F 0 "P6" H 4200 7300 50  0000 C CNN
F 1 "CONN_01X01" V 4300 7200 50  0000 C CNN
F 2 "" H 4200 7200 60  0000 C CNN
F 3 "" H 4200 7200 60  0000 C CNN
	1    4200 7200
	0    -1   -1   0   
$EndComp
$Comp
L CONN_01X01 P7
U 1 1 54B117CA
P 4400 7200
F 0 "P7" H 4400 7300 50  0000 C CNN
F 1 "CONN_01X01" V 4500 7200 50  0000 C CNN
F 2 "" H 4400 7200 60  0000 C CNN
F 3 "" H 4400 7200 60  0000 C CNN
	1    4400 7200
	0    -1   -1   0   
$EndComp
$Comp
L CONN_01X01 P8
U 1 1 54B11854
P 4600 7200
F 0 "P8" H 4600 7300 50  0000 C CNN
F 1 "CONN_01X01" V 4700 7200 50  0000 C CNN
F 2 "" H 4600 7200 60  0000 C CNN
F 3 "" H 4600 7200 60  0000 C CNN
	1    4600 7200
	0    -1   -1   0   
$EndComp
Text Label 3600 7400 3    60   ~ 0
SCL
Text Label 3800 7400 3    60   ~ 0
SDA
Text Label 4000 7400 3    60   ~ 0
PWM
$Comp
L +5V #PWR021
U 1 1 54B11B94
P 4200 7550
F 0 "#PWR021" H 4200 7640 20  0001 C CNN
F 1 "+5V" H 4200 7640 30  0000 C CNN
F 2 "" H 4200 7550 60  0000 C CNN
F 3 "" H 4200 7550 60  0000 C CNN
	1    4200 7550
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR022
U 1 1 54B11BA8
P 4400 7550
F 0 "#PWR022" H 4400 7550 30  0001 C CNN
F 1 "GND" H 4400 7480 30  0001 C CNN
F 2 "" H 4400 7550 60  0000 C CNN
F 3 "" H 4400 7550 60  0000 C CNN
	1    4400 7550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR023
U 1 1 54B11BED
P 4600 7550
F 0 "#PWR023" H 4600 7550 30  0001 C CNN
F 1 "GND" H 4600 7480 30  0001 C CNN
F 2 "" H 4600 7550 60  0000 C CNN
F 3 "" H 4600 7550 60  0000 C CNN
	1    4600 7550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 7400 4200 7550
Wire Wire Line
	4400 7400 4400 7550
Wire Wire Line
	4600 7400 4600 7550
$Comp
L +5V #PWR024
U 1 1 54B12068
P 3600 4600
F 0 "#PWR024" H 3600 4690 20  0001 C CNN
F 1 "+5V" H 3600 4690 30  0000 C CNN
F 2 "" H 3600 4600 60  0000 C CNN
F 3 "" H 3600 4600 60  0000 C CNN
	1    3600 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 4600 3600 4650
Connection ~ 3600 4650
$Comp
L NPN Q4
U 1 1 54B123F2
P 8600 1550
F 0 "Q4" H 8600 1400 50  0000 R CNN
F 1 "PDTC143ET" H 8600 1700 50  0000 R CNN
F 2 "" H 8600 1550 60  0000 C CNN
F 3 "" H 8600 1550 60  0000 C CNN
	1    8600 1550
	1    0    0    -1  
$EndComp
$Comp
L MOSFET_P Q5
U 1 1 54B12463
P 9450 1350
F 0 "Q5" H 9450 1540 60  0000 R CNN
F 1 "IRFR5305" H 9450 1170 60  0000 R CNN
F 2 "" H 9450 1350 60  0000 C CNN
F 3 "" H 9450 1350 60  0000 C CNN
	1    9450 1350
	1    0    0    -1  
$EndComp
$Comp
L MOSFET_N Q6
U 1 1 54B12496
P 9450 2000
F 0 "Q6" H 9460 2170 60  0000 R CNN
F 1 "IRFR1205" H 9460 1850 60  0000 R CNN
F 2 "" H 9450 2000 60  0000 C CNN
F 3 "" H 9450 2000 60  0000 C CNN
	1    9450 2000
	1    0    0    -1  
$EndComp
$Comp
L R R20
U 1 1 54B126C5
P 8000 1550
F 0 "R20" V 8080 1550 40  0000 C CNN
F 1 "1k" V 8007 1551 40  0000 C CNN
F 2 "" V 7930 1550 30  0000 C CNN
F 3 "" H 8000 1550 30  0000 C CNN
	1    8000 1550
	0    1    1    0   
$EndComp
$Comp
L R R24
U 1 1 54B1273B
P 9150 1100
F 0 "R24" V 9230 1100 40  0000 C CNN
F 1 "470" V 9157 1101 40  0000 C CNN
F 2 "" V 9080 1100 30  0000 C CNN
F 3 "" H 9150 1100 30  0000 C CNN
	1    9150 1100
	-1   0    0    1   
$EndComp
$Comp
L R R25
U 1 1 54B127D5
P 9150 2250
F 0 "R25" V 9230 2250 40  0000 C CNN
F 1 "18k" V 9157 2251 40  0000 C CNN
F 2 "" V 9080 2250 30  0000 C CNN
F 3 "" H 9150 2250 30  0000 C CNN
	1    9150 2250
	-1   0    0    1   
$EndComp
$Comp
L VCC #PWR025
U 1 1 54B12849
P 9550 750
F 0 "#PWR025" H 9550 850 30  0001 C CNN
F 1 "VCC" H 9550 850 30  0000 C CNN
F 2 "" H 9550 750 60  0000 C CNN
F 3 "" H 9550 750 60  0000 C CNN
	1    9550 750 
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR026
U 1 1 54B1285D
P 9550 2600
F 0 "#PWR026" H 9550 2600 30  0001 C CNN
F 1 "GND" H 9550 2530 30  0001 C CNN
F 2 "" H 9550 2600 60  0000 C CNN
F 3 "" H 9550 2600 60  0000 C CNN
	1    9550 2600
	1    0    0    -1  
$EndComp
$Comp
L R R23
U 1 1 54B1291A
P 8400 2000
F 0 "R23" V 8480 2000 40  0000 C CNN
F 1 "100" V 8407 2001 40  0000 C CNN
F 2 "" V 8330 2000 30  0000 C CNN
F 3 "" H 8400 2000 30  0000 C CNN
	1    8400 2000
	0    1    1    0   
$EndComp
Text Label 7750 1550 2    60   ~ 0
A+
Text Label 8150 2000 2    60   ~ 0
A-
Wire Wire Line
	8250 1550 8400 1550
$Comp
L GND #PWR027
U 1 1 54B137A7
P 8700 1800
F 0 "#PWR027" H 8700 1800 30  0001 C CNN
F 1 "GND" H 8700 1730 30  0001 C CNN
F 2 "" H 8700 1800 60  0000 C CNN
F 3 "" H 8700 1800 60  0000 C CNN
	1    8700 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8700 1750 8700 1800
Wire Wire Line
	8650 2000 9250 2000
Connection ~ 9150 2000
Wire Wire Line
	9550 2200 9550 2600
Wire Wire Line
	9150 2500 9550 2500
Connection ~ 9550 2500
Wire Wire Line
	8700 1350 9250 1350
Connection ~ 9150 1350
Wire Wire Line
	9550 750  9550 1150
Wire Wire Line
	9150 850  9550 850 
Connection ~ 9550 850 
Wire Wire Line
	9550 1550 9550 1800
Text Label 9550 1650 0    60   ~ 0
PHASE_A
$Comp
L NPN Q7
U 1 1 54B155F4
P 9800 5300
F 0 "Q7" H 9800 5150 50  0000 R CNN
F 1 "PDTC143ET" H 9800 5450 50  0000 R CNN
F 2 "" H 9800 5300 60  0000 C CNN
F 3 "" H 9800 5300 60  0000 C CNN
	1    9800 5300
	1    0    0    -1  
$EndComp
$Comp
L MOSFET_P Q8
U 1 1 54B155FA
P 10650 5100
F 0 "Q8" H 10650 5290 60  0000 R CNN
F 1 "IRFR5305" H 10650 4920 60  0000 R CNN
F 2 "" H 10650 5100 60  0000 C CNN
F 3 "" H 10650 5100 60  0000 C CNN
	1    10650 5100
	1    0    0    -1  
$EndComp
$Comp
L MOSFET_N Q9
U 1 1 54B15600
P 10650 5750
F 0 "Q9" H 10660 5920 60  0000 R CNN
F 1 "IRFR1205" H 10660 5600 60  0000 R CNN
F 2 "" H 10650 5750 60  0000 C CNN
F 3 "" H 10650 5750 60  0000 C CNN
	1    10650 5750
	1    0    0    -1  
$EndComp
$Comp
L R R26
U 1 1 54B15606
P 9200 5300
F 0 "R26" V 9280 5300 40  0000 C CNN
F 1 "1k" V 9207 5301 40  0000 C CNN
F 2 "" V 9130 5300 30  0000 C CNN
F 3 "" H 9200 5300 30  0000 C CNN
	1    9200 5300
	0    1    1    0   
$EndComp
$Comp
L R R28
U 1 1 54B1560C
P 10350 4850
F 0 "R28" V 10430 4850 40  0000 C CNN
F 1 "470" V 10357 4851 40  0000 C CNN
F 2 "" V 10280 4850 30  0000 C CNN
F 3 "" H 10350 4850 30  0000 C CNN
	1    10350 4850
	-1   0    0    1   
$EndComp
$Comp
L R R29
U 1 1 54B15612
P 10350 6000
F 0 "R29" V 10430 6000 40  0000 C CNN
F 1 "18k" V 10357 6001 40  0000 C CNN
F 2 "" V 10280 6000 30  0000 C CNN
F 3 "" H 10350 6000 30  0000 C CNN
	1    10350 6000
	-1   0    0    1   
$EndComp
$Comp
L VCC #PWR028
U 1 1 54B15618
P 10750 4500
F 0 "#PWR028" H 10750 4600 30  0001 C CNN
F 1 "VCC" H 10750 4600 30  0000 C CNN
F 2 "" H 10750 4500 60  0000 C CNN
F 3 "" H 10750 4500 60  0000 C CNN
	1    10750 4500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR029
U 1 1 54B1561E
P 10750 6350
F 0 "#PWR029" H 10750 6350 30  0001 C CNN
F 1 "GND" H 10750 6280 30  0001 C CNN
F 2 "" H 10750 6350 60  0000 C CNN
F 3 "" H 10750 6350 60  0000 C CNN
	1    10750 6350
	1    0    0    -1  
$EndComp
$Comp
L R R27
U 1 1 54B15624
P 9600 5750
F 0 "R27" V 9680 5750 40  0000 C CNN
F 1 "100" V 9607 5751 40  0000 C CNN
F 2 "" V 9530 5750 30  0000 C CNN
F 3 "" H 9600 5750 30  0000 C CNN
	1    9600 5750
	0    1    1    0   
$EndComp
Text Label 8950 5300 2    60   ~ 0
C+
Text Label 9350 5750 2    60   ~ 0
C-
Wire Wire Line
	9450 5300 9600 5300
$Comp
L GND #PWR030
U 1 1 54B1562D
P 9900 5550
F 0 "#PWR030" H 9900 5550 30  0001 C CNN
F 1 "GND" H 9900 5480 30  0001 C CNN
F 2 "" H 9900 5550 60  0000 C CNN
F 3 "" H 9900 5550 60  0000 C CNN
	1    9900 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	9850 5750 10450 5750
Connection ~ 10350 5750
Wire Wire Line
	10750 5950 10750 6350
Wire Wire Line
	10350 6250 10750 6250
Connection ~ 10750 6250
Wire Wire Line
	9900 5100 10450 5100
Connection ~ 10350 5100
Wire Wire Line
	10750 4500 10750 4900
Wire Wire Line
	10350 4600 10750 4600
Connection ~ 10750 4600
Wire Wire Line
	10750 5300 10750 5550
Text Label 10750 5400 0    60   ~ 0
PHASE_C
$Comp
L NPN Q1
U 1 1 54B158B3
P 7650 3750
F 0 "Q1" H 7650 3600 50  0000 R CNN
F 1 "PDTC143ET" H 7650 3900 50  0000 R CNN
F 2 "" H 7650 3750 60  0000 C CNN
F 3 "" H 7650 3750 60  0000 C CNN
	1    7650 3750
	1    0    0    -1  
$EndComp
$Comp
L MOSFET_P Q2
U 1 1 54B158B9
P 8500 3550
F 0 "Q2" H 8500 3740 60  0000 R CNN
F 1 "IRFR5305" H 8500 3370 60  0000 R CNN
F 2 "" H 8500 3550 60  0000 C CNN
F 3 "" H 8500 3550 60  0000 C CNN
	1    8500 3550
	1    0    0    -1  
$EndComp
$Comp
L MOSFET_N Q3
U 1 1 54B158BF
P 8500 4200
F 0 "Q3" H 8510 4370 60  0000 R CNN
F 1 "IRFR1205" H 8510 4050 60  0000 R CNN
F 2 "" H 8500 4200 60  0000 C CNN
F 3 "" H 8500 4200 60  0000 C CNN
	1    8500 4200
	1    0    0    -1  
$EndComp
$Comp
L R R18
U 1 1 54B158C5
P 7050 3750
F 0 "R18" V 7130 3750 40  0000 C CNN
F 1 "1k" V 7057 3751 40  0000 C CNN
F 2 "" V 6980 3750 30  0000 C CNN
F 3 "" H 7050 3750 30  0000 C CNN
	1    7050 3750
	0    1    1    0   
$EndComp
$Comp
L R R21
U 1 1 54B158CB
P 8200 3300
F 0 "R21" V 8280 3300 40  0000 C CNN
F 1 "470" V 8207 3301 40  0000 C CNN
F 2 "" V 8130 3300 30  0000 C CNN
F 3 "" H 8200 3300 30  0000 C CNN
	1    8200 3300
	-1   0    0    1   
$EndComp
$Comp
L R R22
U 1 1 54B158D1
P 8200 4450
F 0 "R22" V 8280 4450 40  0000 C CNN
F 1 "18k" V 8207 4451 40  0000 C CNN
F 2 "" V 8130 4450 30  0000 C CNN
F 3 "" H 8200 4450 30  0000 C CNN
	1    8200 4450
	-1   0    0    1   
$EndComp
$Comp
L VCC #PWR031
U 1 1 54B158D7
P 8600 2950
F 0 "#PWR031" H 8600 3050 30  0001 C CNN
F 1 "VCC" H 8600 3050 30  0000 C CNN
F 2 "" H 8600 2950 60  0000 C CNN
F 3 "" H 8600 2950 60  0000 C CNN
	1    8600 2950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR032
U 1 1 54B158DD
P 8600 4800
F 0 "#PWR032" H 8600 4800 30  0001 C CNN
F 1 "GND" H 8600 4730 30  0001 C CNN
F 2 "" H 8600 4800 60  0000 C CNN
F 3 "" H 8600 4800 60  0000 C CNN
	1    8600 4800
	1    0    0    -1  
$EndComp
$Comp
L R R19
U 1 1 54B158E3
P 7450 4200
F 0 "R19" V 7530 4200 40  0000 C CNN
F 1 "100" V 7457 4201 40  0000 C CNN
F 2 "" V 7380 4200 30  0000 C CNN
F 3 "" H 7450 4200 30  0000 C CNN
	1    7450 4200
	0    1    1    0   
$EndComp
Text Label 6800 3750 2    60   ~ 0
B+
Text Label 7200 4200 2    60   ~ 0
B-
Wire Wire Line
	7300 3750 7450 3750
$Comp
L GND #PWR033
U 1 1 54B158EC
P 7750 4000
F 0 "#PWR033" H 7750 4000 30  0001 C CNN
F 1 "GND" H 7750 3930 30  0001 C CNN
F 2 "" H 7750 4000 60  0000 C CNN
F 3 "" H 7750 4000 60  0000 C CNN
	1    7750 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 3950 7750 4000
Wire Wire Line
	7700 4200 8300 4200
Connection ~ 8200 4200
Wire Wire Line
	8600 4400 8600 4800
Wire Wire Line
	8200 4700 8600 4700
Connection ~ 8600 4700
Wire Wire Line
	7750 3550 8300 3550
Connection ~ 8200 3550
Wire Wire Line
	8200 3050 8600 3050
Connection ~ 8600 3050
Wire Wire Line
	8600 3750 8600 4000
Text Label 8600 3850 0    60   ~ 0
PHASE_B
Wire Wire Line
	1150 1650 1150 1700
Wire Wire Line
	1950 950  1950 900 
Wire Wire Line
	1150 2700 1150 2650
Wire Wire Line
	1850 1700 2150 1700
Wire Wire Line
	9900 5500 9900 5550
Text Label 1950 6550 2    60   ~ 0
TXD
Wire Wire Line
	8600 2950 8600 3350
$Comp
L CONN_1 P10
U 1 1 54B2CB27
P 9800 1750
F 0 "P10" H 9880 1750 40  0000 L CNN
F 1 "CONN_1" H 9800 1805 30  0001 C CNN
F 2 "" H 9800 1750 60  0000 C CNN
F 3 "" H 9800 1750 60  0000 C CNN
	1    9800 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	9650 1750 9550 1750
Connection ~ 9550 1750
$Comp
L CONN_1 P9
U 1 1 54B2CCE9
P 8800 3950
F 0 "P9" H 8880 3950 40  0000 L CNN
F 1 "CONN_1" H 8800 4005 30  0001 C CNN
F 2 "" H 8800 3950 60  0000 C CNN
F 3 "" H 8800 3950 60  0000 C CNN
	1    8800 3950
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 P11
U 1 1 54B2CCEF
P 10950 5500
F 0 "P11" H 11030 5500 40  0000 L CNN
F 1 "CONN_1" H 10950 5555 30  0001 C CNN
F 2 "" H 10950 5500 60  0000 C CNN
F 3 "" H 10950 5500 60  0000 C CNN
	1    10950 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	8650 3950 8600 3950
Connection ~ 8600 3950
Wire Wire Line
	10800 5500 10750 5500
Connection ~ 10750 5500
$EndSCHEMATC
