EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:quadcopter_custom
LIBS:rocket_booster-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title ""
Date "12 jan 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L NHD-C12832A1Z U7
U 1 1 54B35A5F
P 5450 3550
F 0 "U7" H 5450 2900 60  0000 C CNN
F 1 "NHD-C12832A1Z" H 5400 4000 60  0000 C CNN
F 2 "qustom_footprints:NHD-C12832A1Z" H 5450 3550 60  0001 C CNN
F 3 "" H 5450 3550 60  0000 C CNN
	1    5450 3550
	1    0    0    -1  
$EndComp
$Comp
L VDD #PWR057
U 1 1 54B35BBB
P 6750 3550
F 0 "#PWR057" H 6750 3650 30  0001 C CNN
F 1 "VDD" H 6750 3660 30  0000 C CNN
F 2 "" H 6750 3550 60  0000 C CNN
F 3 "" H 6750 3550 60  0000 C CNN
	1    6750 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 3600 6750 3600
Wire Wire Line
	6750 3600 6750 3550
$Comp
L GND #PWR058
U 1 1 54B35D1B
P 6600 3500
F 0 "#PWR058" H 6600 3500 30  0001 C CNN
F 1 "GND" H 6600 3430 30  0001 C CNN
F 2 "" H 6600 3500 60  0000 C CNN
F 3 "" H 6600 3500 60  0000 C CNN
	1    6600 3500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6000 3500 6600 3500
$Comp
L C C23
U 1 1 54B35E84
P 4450 4000
F 0 "C23" H 4450 4100 40  0000 L CNN
F 1 "0.47uF" H 4456 3915 40  0000 L CNN
F 2 "SMD_Packages:SMD-0603_c" H 4488 3850 30  0000 C CNN
F 3 "~" H 4450 4000 60  0000 C CNN
	1    4450 4000
	1    0    0    -1  
$EndComp
$Comp
L C C22
U 1 1 54B35E91
P 4250 4000
F 0 "C22" H 4250 4100 40  0000 L CNN
F 1 "0.47uF" H 4256 3915 40  0000 L CNN
F 2 "SMD_Packages:SMD-0603_c" H 4288 3850 30  0000 C CNN
F 3 "~" H 4250 4000 60  0000 C CNN
	1    4250 4000
	1    0    0    -1  
$EndComp
$Comp
L C C21
U 1 1 54B35E97
P 4050 4000
F 0 "C21" H 4050 4100 40  0000 L CNN
F 1 "0.47uF" H 4056 3915 40  0000 L CNN
F 2 "SMD_Packages:SMD-0603_c" H 4088 3850 30  0000 C CNN
F 3 "~" H 4050 4000 60  0000 C CNN
	1    4050 4000
	1    0    0    -1  
$EndComp
$Comp
L C C20
U 1 1 54B35E9D
P 3850 4000
F 0 "C20" H 3850 4100 40  0000 L CNN
F 1 "0.47uF" H 3856 3915 40  0000 L CNN
F 2 "SMD_Packages:SMD-0603_c" H 3888 3850 30  0000 C CNN
F 3 "~" H 3850 4000 60  0000 C CNN
	1    3850 4000
	1    0    0    -1  
$EndComp
$Comp
L C C19
U 1 1 54B35EA3
P 3650 4000
F 0 "C19" H 3650 4100 40  0000 L CNN
F 1 "0.47uF" H 3656 3915 40  0000 L CNN
F 2 "SMD_Packages:SMD-0603_c" H 3688 3850 30  0000 C CNN
F 3 "~" H 3650 4000 60  0000 C CNN
	1    3650 4000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR059
U 1 1 54B35FF8
P 4050 4250
F 0 "#PWR059" H 4050 4250 30  0001 C CNN
F 1 "GND" H 4050 4180 30  0001 C CNN
F 2 "" H 4050 4250 60  0000 C CNN
F 3 "" H 4050 4250 60  0000 C CNN
	1    4050 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 4200 4450 4200
Connection ~ 3850 4200
Connection ~ 4050 4200
Connection ~ 4250 4200
Wire Wire Line
	4050 4250 4050 4200
Wire Wire Line
	4450 3800 4900 3800
Wire Wire Line
	4900 3700 4250 3700
Wire Wire Line
	4250 3700 4250 3800
Wire Wire Line
	4050 3800 4050 3600
Wire Wire Line
	4050 3600 4900 3600
Wire Wire Line
	4900 3500 3850 3500
Wire Wire Line
	3850 3500 3850 3800
Wire Wire Line
	4900 3400 3650 3400
Wire Wire Line
	3650 3400 3650 3800
$Comp
L C C24
U 1 1 54B362FA
P 4650 4150
F 0 "C24" H 4650 4250 40  0000 L CNN
F 1 "1uF" H 4656 4065 40  0000 L CNN
F 2 "SMD_Packages:SMD-0603_c" H 4688 4000 30  0000 C CNN
F 3 "~" H 4650 4150 60  0000 C CNN
	1    4650 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 3900 4650 3900
Wire Wire Line
	4650 3900 4650 3950
Wire Wire Line
	4900 4000 4900 4350
Wire Wire Line
	4900 4350 4650 4350
$Comp
L C C25
U 1 1 54B364A3
P 6300 3200
F 0 "C25" H 6300 3300 40  0000 L CNN
F 1 "1uF" H 6306 3115 40  0000 L CNN
F 2 "SMD_Packages:SMD-0603_c" H 6338 3050 30  0000 C CNN
F 3 "~" H 6300 3200 60  0000 C CNN
	1    6300 3200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6000 3200 6100 3200
Wire Wire Line
	6000 3300 6500 3300
Wire Wire Line
	6500 3300 6500 3200
$Comp
L C C26
U 1 1 54B36623
P 6300 3400
F 0 "C26" H 6300 3500 40  0000 L CNN
F 1 "1uF" H 6306 3315 40  0000 L CNN
F 2 "SMD_Packages:SMD-0603_c" H 6338 3250 30  0000 C CNN
F 3 "~" H 6300 3400 60  0000 C CNN
	1    6300 3400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6000 3400 6100 3400
Wire Wire Line
	6500 3350 6500 3500
Connection ~ 6500 3500
$Comp
L GND #PWR060
U 1 1 54B367CE
P 4650 3300
F 0 "#PWR060" H 4650 3300 30  0001 C CNN
F 1 "GND" H 4650 3230 30  0001 C CNN
F 2 "" H 4650 3300 60  0000 C CNN
F 3 "" H 4650 3300 60  0000 C CNN
	1    4650 3300
	0    1    1    0   
$EndComp
$Comp
L +3.3V #PWR061
U 1 1 54B367DD
P 4650 3200
F 0 "#PWR061" H 4650 3160 30  0001 C CNN
F 1 "+3.3V" H 4650 3310 30  0000 C CNN
F 2 "" H 4650 3200 60  0000 C CNN
F 3 "" H 4650 3200 60  0000 C CNN
	1    4650 3200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4900 3300 4650 3300
Wire Wire Line
	4650 3200 4900 3200
Text HLabel 6500 3700 2    60   BiDi ~ 0
iNEMO_SL
Text HLabel 6500 3800 2    60   Input ~ 0
iNEMO_SCL
Text HLabel 6500 3900 2    60   Input ~ 0
iNEMO_A0
Text HLabel 6500 4000 2    60   Input ~ 0
iNEMO_nRST
Text HLabel 6500 4100 2    60   Input ~ 0
iNEMO_nCS1
Wire Wire Line
	6000 3700 6500 3700
Wire Wire Line
	6500 3800 6000 3800
Wire Wire Line
	6000 3900 6500 3900
Wire Wire Line
	6000 4000 6500 4000
Wire Wire Line
	6500 4100 6000 4100
Text HLabel 4750 3050 0    60   Input ~ 0
iNEMO_3V
Text HLabel 6550 3350 2    60   Input ~ 0
iNEMO_GND
Wire Wire Line
	4750 3050 4800 3050
Wire Wire Line
	4800 3050 4800 3200
Connection ~ 4800 3200
Wire Wire Line
	6500 3350 6550 3350
Connection ~ 6500 3400
$EndSCHEMATC
