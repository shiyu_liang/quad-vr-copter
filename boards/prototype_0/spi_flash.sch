EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:quadcopter_custom
LIBS:prototype_0-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 5
Title ""
Date "8 jan 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L M25PX80 IC?
U 1 1 54AE6225
P 5200 3550
F 0 "IC?" H 5200 3300 60  0000 C CNN
F 1 "M25PX80" H 5200 3800 60  0000 C CNN
F 2 "~" H 5200 3550 60  0000 C CNN
F 3 "~" H 5200 3550 60  0000 C CNN
	1    5200 3550
	1    0    0    -1  
$EndComp
Text HLabel 4150 3400 0    60   Input ~ 0
SFL_CS
Text HLabel 4150 3500 0    60   Output ~ 0
SFL_DIN
Text HLabel 4150 3600 0    60   Input ~ 0
SFL_DOUT
Text HLabel 4150 3700 0    60   Input ~ 0
SFL_CLK
Wire Wire Line
	4150 3400 4600 3400
Wire Wire Line
	4600 3500 4150 3500
Wire Wire Line
	4150 3600 4600 3600
Wire Wire Line
	4600 3700 4150 3700
$Comp
L +3.3V #PWR?
U 1 1 54AE624B
P 6050 3150
F 0 "#PWR?" H 6050 3110 30  0001 C CNN
F 1 "+3.3V" H 6050 3260 30  0000 C CNN
F 2 "" H 6050 3150 60  0000 C CNN
F 3 "" H 6050 3150 60  0000 C CNN
	1    6050 3150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 54AE625A
P 6050 3800
F 0 "#PWR?" H 6050 3800 30  0001 C CNN
F 1 "GND" H 6050 3730 30  0001 C CNN
F 2 "" H 6050 3800 60  0000 C CNN
F 3 "" H 6050 3800 60  0000 C CNN
	1    6050 3800
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 54AE6269
P 6300 3600
F 0 "C?" H 6300 3700 40  0000 L CNN
F 1 "0.1uF" H 6306 3515 40  0000 L CNN
F 2 "~" H 6338 3450 30  0000 C CNN
F 3 "~" H 6300 3600 60  0000 C CNN
	1    6300 3600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 54AE6276
P 6300 3800
F 0 "#PWR?" H 6300 3800 30  0001 C CNN
F 1 "GND" H 6300 3730 30  0001 C CNN
F 2 "" H 6300 3800 60  0000 C CNN
F 3 "" H 6300 3800 60  0000 C CNN
	1    6300 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 3400 5800 3600
Connection ~ 5800 3500
Wire Wire Line
	5800 3400 6300 3400
Wire Wire Line
	6050 3150 6050 3400
Connection ~ 6050 3400
Wire Wire Line
	5800 3700 6050 3700
Wire Wire Line
	6050 3700 6050 3800
$EndSCHEMATC
