EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:quadcopter_custom
LIBS:prototype_0-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 5
Title ""
Date "8 jan 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L LSM9DS1 U?
U 1 1 54AF9B81
P 3500 2000
F 0 "U?" H 3900 2100 60  0000 C CNN
F 1 "LSM9DS1" H 3900 1950 60  0000 C CNN
F 2 "" H 3500 2000 60  0000 C CNN
F 3 "" H 3500 2000 60  0000 C CNN
	1    3500 2000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 54AF9B9A
P 3800 3350
F 0 "#PWR?" H 3800 3350 30  0001 C CNN
F 1 "GND" H 3800 3280 30  0001 C CNN
F 2 "" H 3800 3350 60  0000 C CNN
F 3 "" H 3800 3350 60  0000 C CNN
	1    3800 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 3250 3800 3250
Connection ~ 3300 3250
Connection ~ 3400 3250
Connection ~ 3500 3250
Connection ~ 3600 3250
Connection ~ 3700 3250
Wire Wire Line
	3800 3250 3800 3350
$Comp
L C C?
U 1 1 54AF9D6F
P 3850 1150
F 0 "C?" H 3850 1250 40  0000 L CNN
F 1 "100nF" H 3856 1065 40  0000 L CNN
F 2 "~" H 3888 1000 30  0000 C CNN
F 3 "~" H 3850 1150 60  0000 C CNN
	1    3850 1150
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 54AF9D7C
P 4050 1150
F 0 "C?" H 4050 1250 40  0000 L CNN
F 1 "10uF" H 4056 1065 40  0000 L CNN
F 2 "~" H 4088 1000 30  0000 C CNN
F 3 "~" H 4050 1150 60  0000 C CNN
	1    4050 1150
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR?
U 1 1 54AF9D8E
P 3950 850
F 0 "#PWR?" H 3950 810 30  0001 C CNN
F 1 "+3.3V" H 3950 960 30  0000 C CNN
F 2 "" H 3950 850 60  0000 C CNN
F 3 "" H 3950 850 60  0000 C CNN
	1    3950 850 
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 1250 3650 1250
Wire Wire Line
	3950 850  3950 950 
Wire Wire Line
	3850 950  4050 950 
Connection ~ 3950 950 
Wire Wire Line
	3950 900  3650 900 
Wire Wire Line
	3650 900  3650 1250
Connection ~ 3950 900 
$Comp
L GND #PWR?
U 1 1 54AF9DC5
P 3950 1400
F 0 "#PWR?" H 3950 1400 30  0001 C CNN
F 1 "GND" H 3950 1330 30  0001 C CNN
F 2 "" H 3950 1400 60  0000 C CNN
F 3 "" H 3950 1400 60  0000 C CNN
	1    3950 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 1350 4050 1350
Wire Wire Line
	3950 1400 3950 1350
Connection ~ 3950 1350
$Comp
L C C?
U 1 1 54AF9FAF
P 3050 1200
F 0 "C?" H 3050 1300 40  0000 L CNN
F 1 "100nF" H 3056 1115 40  0000 L CNN
F 2 "~" H 3088 1050 30  0000 C CNN
F 3 "~" H 3050 1200 60  0000 C CNN
	1    3050 1200
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 54AF9FB5
P 3050 1450
F 0 "#PWR?" H 3050 1450 30  0001 C CNN
F 1 "GND" H 3050 1380 30  0001 C CNN
F 2 "" H 3050 1450 60  0000 C CNN
F 3 "" H 3050 1450 60  0000 C CNN
	1    3050 1450
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR?
U 1 1 54AF9FBD
P 3050 950
F 0 "#PWR?" H 3050 910 30  0001 C CNN
F 1 "+3.3V" H 3050 1060 30  0000 C CNN
F 2 "" H 3050 950 60  0000 C CNN
F 3 "" H 3050 950 60  0000 C CNN
	1    3050 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 1250 3350 1250
Wire Wire Line
	3050 950  3050 1000
Wire Wire Line
	3050 1400 3050 1450
Wire Wire Line
	3050 1000 3350 1000
Wire Wire Line
	3350 1000 3350 1250
$Comp
L C C?
U 1 1 54AFA1BE
P 4500 2800
F 0 "C?" H 4500 2900 40  0000 L CNN
F 1 "100nF" H 4506 2715 40  0000 L CNN
F 2 "~" H 4538 2650 30  0000 C CNN
F 3 "~" H 4500 2800 60  0000 C CNN
	1    4500 2800
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 54AFA1C4
P 4300 2900
F 0 "C?" H 4300 3000 40  0000 L CNN
F 1 "10nF" H 4306 2815 40  0000 L CNN
F 2 "~" H 4338 2750 30  0000 C CNN
F 3 "~" H 4300 2900 60  0000 C CNN
	1    4300 2900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 54AFA1D6
P 4300 3150
F 0 "#PWR?" H 4300 3150 30  0001 C CNN
F 1 "GND" H 4300 3080 30  0001 C CNN
F 2 "" H 4300 3150 60  0000 C CNN
F 3 "" H 4300 3150 60  0000 C CNN
	1    4300 3150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 54AFA1DC
P 4500 3050
F 0 "#PWR?" H 4500 3050 30  0001 C CNN
F 1 "GND" H 4500 2980 30  0001 C CNN
F 2 "" H 4500 3050 60  0000 C CNN
F 3 "" H 4500 3050 60  0000 C CNN
	1    4500 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 2600 4500 2600
Wire Wire Line
	4500 3000 4500 3050
Wire Wire Line
	4200 2700 4300 2700
Wire Wire Line
	4300 3100 4300 3150
Text HLabel 1150 1650 0    60   BiDi Italic 0
I2C_SDA
Text HLabel 1150 1750 0    60   BiDi Italic 0
I2C_SCL
$Comp
L R R?
U 1 1 54AFB15D
P 2100 2750
F 0 "R?" V 2180 2750 40  0000 C CNN
F 1 "10k" V 2107 2751 40  0000 C CNN
F 2 "~" V 2030 2750 30  0000 C CNN
F 3 "~" H 2100 2750 30  0000 C CNN
	1    2100 2750
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR?
U 1 1 54AFB16C
P 2100 2400
F 0 "#PWR?" H 2100 2360 30  0001 C CNN
F 1 "+3.3V" H 2100 2510 30  0000 C CNN
F 2 "" H 2100 2400 60  0000 C CNN
F 3 "" H 2100 2400 60  0000 C CNN
	1    2100 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 2400 2100 2500
Wire Wire Line
	2100 3000 2100 3050
Wire Wire Line
	2100 3050 2650 3050
Wire Wire Line
	2650 3050 2650 2700
Wire Wire Line
	2650 2700 2750 2700
NoConn ~ 2750 2600
NoConn ~ 2750 2500
$Comp
L R R?
U 1 1 54AFB50B
P 4800 2100
F 0 "R?" V 4880 2100 40  0000 C CNN
F 1 "10k" V 4807 2101 40  0000 C CNN
F 2 "~" V 4730 2100 30  0000 C CNN
F 3 "~" H 4800 2100 30  0000 C CNN
	1    4800 2100
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR?
U 1 1 54AFB511
P 4800 1800
F 0 "#PWR?" H 4800 1760 30  0001 C CNN
F 1 "+3.3V" H 4800 1910 30  0000 C CNN
F 2 "" H 4800 1800 60  0000 C CNN
F 3 "" H 4800 1800 60  0000 C CNN
	1    4800 1800
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 54AFB517
P 4950 2200
F 0 "R?" V 5030 2200 40  0000 C CNN
F 1 "10k" V 4957 2201 40  0000 C CNN
F 2 "~" V 4880 2200 30  0000 C CNN
F 3 "~" H 4950 2200 30  0000 C CNN
	1    4950 2200
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR?
U 1 1 54AFB51D
P 4950 1900
F 0 "#PWR?" H 4950 1860 30  0001 C CNN
F 1 "+3.3V" H 4950 2010 30  0000 C CNN
F 2 "" H 4950 1900 60  0000 C CNN
F 3 "" H 4950 1900 60  0000 C CNN
	1    4950 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 1800 4800 1850
Wire Wire Line
	4950 1900 4950 1950
Wire Wire Line
	4200 2400 4800 2400
Wire Wire Line
	4800 2400 4800 2350
Wire Wire Line
	4200 2500 4950 2500
Wire Wire Line
	4950 2500 4950 2450
Text Notes 4300 2400 0    60   Italic 0
use I2C for accelerometer/gyro
Text Notes 4300 2500 0    60   Italic 0
use I2C for magnetometer
NoConn ~ 2750 2400
NoConn ~ 2750 2300
Text Label 1500 1650 0    60   Italic 0
g_sda
Text Label 1500 1750 0    60   Italic 0
g_scl
Wire Wire Line
	1150 1650 1900 1650
Wire Wire Line
	1150 1750 1800 1750
Text Label 2350 1900 0    60   Italic 0
g_sda
Text Label 2350 2000 0    60   Italic 0
g_scl
Wire Wire Line
	1900 1900 2750 1900
Wire Wire Line
	1800 2000 2750 2000
$Comp
L R R?
U 1 1 54AFBD0D
P 1200 1300
F 0 "R?" V 1280 1300 40  0000 C CNN
F 1 "3.3k" V 1207 1301 40  0000 C CNN
F 2 "~" V 1130 1300 30  0000 C CNN
F 3 "~" H 1200 1300 30  0000 C CNN
	1    1200 1300
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 54AFBD1A
P 1350 1300
F 0 "R?" V 1430 1300 40  0000 C CNN
F 1 "3.3k" V 1357 1301 40  0000 C CNN
F 2 "~" V 1280 1300 30  0000 C CNN
F 3 "~" H 1350 1300 30  0000 C CNN
	1    1350 1300
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR?
U 1 1 54AFBD44
P 1200 1000
F 0 "#PWR?" H 1200 960 30  0001 C CNN
F 1 "+3.3V" H 1200 1110 30  0000 C CNN
F 2 "" H 1200 1000 60  0000 C CNN
F 3 "" H 1200 1000 60  0000 C CNN
	1    1200 1000
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR?
U 1 1 54AFBD51
P 1350 1000
F 0 "#PWR?" H 1350 960 30  0001 C CNN
F 1 "+3.3V" H 1350 1110 30  0000 C CNN
F 2 "" H 1350 1000 60  0000 C CNN
F 3 "" H 1350 1000 60  0000 C CNN
	1    1350 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 1550 1200 1650
Connection ~ 1200 1650
Wire Wire Line
	1350 1550 1350 1750
Connection ~ 1350 1750
Wire Wire Line
	1200 1000 1200 1050
Wire Wire Line
	1350 1000 1350 1050
Wire Wire Line
	1900 1650 1900 1900
Wire Wire Line
	1800 1750 1800 2000
$EndSCHEMATC
