#!/usr/bin/python

# turn XML BOM format to CSV format
# columns are arranged as follows
# partID,footprint,count

import sys
import xml.etree.ElementTree as ET

print "parsing ", sys.argv[1]
tree = ET.parse (sys.argv[1])
root = tree.getroot()

parts = {}

for c in root.iter('comp'):
        name = c.get('ref')
        value = c.find('value').text

        footprint = c.find('footprint')

        if footprint is not None:
                ft = footprint.text
        else:
                ft = "N/A"
        print name, value, ft
        key = value + ',' + ft
        if key in parts:
                parts[key] += 1
        else:
                parts[key] = 1

for key in parts:
        print key+','+str(parts[key])
