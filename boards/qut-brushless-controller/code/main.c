/* 
 * qut-brushless-controller, an open-source Brushless DC motor controller
 * Copyright (C) 2011 Toby Lockley <tobylockley@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//===================================//
//      Main Controller File         //
//===================================//
//Initialisation and main program code
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

//Pin definitions and config files
#include "config/BLDC_pindefs_breadboard.h"
#include "config/BLDC_config.h"
//#include "config/BLDC_motor_A2208.h"
//#include "config/BLDC_motor_smallpink.h"
//#include "config/BLDC_motor_smallsilver.h"
//#include "config/BLDC_motor_HDD.h"
#include "config/BLDC_motor_A1510.h"

//Controller is broken up into easy to manage stubs
#include "rc_signal.h"
#include "debug.h"

#define TWI_ADDR 0x52

void setPWM(uint8_t val);
void startMotor(void);
void stopMotor(void);
void nextCommutation(void);
void clrAllOutputs(void);
void init_mosfets(void);
void init_registers(void);
void init_twi(void);

extern volatile	uint16_t 	signalBuffer; //See rc_signal.h
volatile uint8_t TWI_data;
volatile uint8_t TWI_update;

volatile uint8_t 	zcActive; //Whether ZC detection is active or not
volatile uint8_t 	zcACO; //What the ACO reading should be after a ZC
volatile uint8_t 	zcTime; //Timer ticks when ZC was detected
volatile uint8_t	currACO; //used to read ACO output during spike rejection
volatile uint8_t 	t1_comp;
volatile uint8_t 	t1_ovfs;
volatile uint8_t 	startupState;
volatile uint8_t 	pwmVal; //0->255
volatile uint8_t 	pwmPhase; //0=NONE, 1=A, 2=B, 3=C
volatile uint8_t 	motorON; //0=off, 1=on
volatile uint8_t 	commState; //1->6, then loops

int main(void)
{

	unsigned int i;
	uint16_t signal;
	
	init_debug();
	init_mosfets();
	init_registers();

	USART_SendString("Starting ESC Test\r\n");

	init_twi();
	USART_SendString("Waiting for interrupt to occur\r\n");

	switch (INPUT_SIGNAL_MODE) {
	case 1:
		init_rc();
		break;
	case 2:
		init_twi();
		break;
	case 3:
		//init_uart();
		break;
	default:
		//NO INPUT SELECTED//
		break;
	}
	
	//Initialise variables
	motorON = 0;
	pwmVal = 0;

	signal = 25;
	TWI_data = 0;
	TWI_update = 0;

	USART_SendString("Entering main control loop\r\n");

	while(1) {
		//if (signalBuffer) { //When signal buffer is populated, update PWM
		if (signal) { //When signal buffer is populated, update PWM
			//USART_SendString("signal set on startup\r\n");
			//pwmVal = processRCSignal(signalBuffer);
			//pwmVal = processRCSignal(signal);
			pwmVal = 30000;
			//signalBuffer = 0; //Clear signal
			signal = 0;
			
			if (pwmVal > PWM_ON_THR) {
				//USART_SendString("starting motor\r\n");
				startMotor();
				motorON = 1; //Enter 'motor on' loop below
			}
		}
		
		while (motorON) {
			//USART_SendString("motor on, speed = ");
			//USART_SendInt(pwmVal);
			//USART_SendString("\r\n");

			//if (signalBuffer) { //When signal buffer is populated, update PWM
			//if (signal) { //When signal buffer is populated, update PWM
			if (TWI_update) { //When signal buffer is populated, update PWM
				//USART_SendString("TWI updated: changing motor speed\r\n");
				//pwmVal = processRCSignal(signalBuffer);
				//pwmVal = processRCSignal(signal);
				//pwmVal = 20000;
				pwmVal = TWI_data;
				//signalBuffer = 0; //Clear signal
				//signal = 0; //Clear signal
				TWI_update = 0;
				///setPWM(pwmVal);
				OCR1A = pwmVal * 1000;

				//OCR1A -= TWI_data;
				
				if (pwmVal < PWM_OFF_THR) {
					//turn off motor (allow slow braking), disable pwm
					TIMER1_STOP();
					PWM_STOP();
					setPWM(0);
					clrAllOutputs();
					motorON = 0; //Leave 'motor on' loop
				}
			}
		}
	}
}


ISR(TIMER0_COMPA_vect)
{
}

ISR(TIMER1_CAPT_vect)
{
	setLED();
	zcTime = ICR1;
	OCR1B = zcTime + ZC_NOISE_REJECT;
	clrLED();
}

ISR(TIMER1_COMPA_vect)
{
	if (startupState) {
		OCR1A *= (1 - STARTUP_RAMP);
	}
	else {
#if defined(__AVR_ATmega8__)
		TIMSK &= ~(1 << OCIE1A); //disable commutation interrupt
#elif defined(__AVR_ATmega328P__)
		TIMSK1 &= ~(1 << OCIE1A); //disable commutation interrupt
#endif
	}
	nextCommutation();
	TCNT1 = 0;
}

ISR(TIMER1_COMPB_vect)
{
	if (zcActive) {
		//Input capture interrupt is set, so zero crossing was detected
		currACO = GET_ACO();
		if (currACO == zcACO) {
			//Analog comparator output has remained the same since interrupt occured, zero crossing has been detected
			ZC_STOP_DETECT();
			zcActive = 0;
#if defined(__AVR_ATmega8__)
			TIFR |= (1 << ICF1); //Make sure input capture flag is cleared by writing logic 1 to it
			OCR1A = 2 * zcTime; //Set commutation time to twice the ZC time
			ENABLE_COMM(); //enable commutation to occur
#elif defined(__AVR_ATmega328P__)
			TIFR1 |= (1 << ICF1);
#endif
			
			OCR1A = 2 * zcTime; //Set commutation time to twice the ZC time
			ENABLE_COMM(); //enable commutation to occur
		}
	}
	else {
		//Input capture interrupt is disabled, so blanking period is in process
		ZC_START_DETECT();
		zcActive = 1;
	}
}

ISR(TIMER1_OVF_vect)
{
	t1_ovfs++; //Keep track of how many times timer1 overflows
}

#if defined(__AVR_ATmega8__)
ISR(TIMER2_COMP_vect)
#elif defined(__AVR_ATmega328P__)
ISR(TIMER2_COMPA_vect)
#endif
{
	CLR_A_LOW();
	CLR_B_LOW();
	CLR_C_LOW();
}

ISR(TIMER2_OVF_vect)
{
	switch (pwmPhase) {
	case 1:
		SET_A_LOW();
		break;
	case 2:
		SET_B_LOW();
		break;
	case 3:
		SET_C_LOW();
		break;
	}
}

#if defined(__AVR_ATmega8__)
ISR(ANA_COMP_vect)
#elif defined(__AVR_ATmega328P__)
ISR(ANALOG_COMP_vect)
#endif
{
	//PORTB ^= (1 << PB1);
	uint8_t temp = ACSR & (1 << ACO);
	PORTB = ((temp >> ACO) << PB1);
}

ISR(TWI_vect)
{
	uint8_t data;
	uint8_t status;
	//TWCR &= ~(1 << TWINT); // disable TWI interrupt
	status = TWSR & 0xF8;
	switch (status)
	{
		case 0x60:
			break;
		case 0x80:
			TWI_data = TWDR;
			break;
		case 0xa0:
		/*
			USART_SendString("received data = ");
			USART_SendHex(TWI_data);
			USART_SendString("\r\n");
		*/
			TWI_update = 1;
			break;
	}
	TWCR |= (1 << TWINT); // enable TWI interrupt
}

void setPWM(uint8_t val)
{
#if defined(__AVR_ATmega8__)
	OCR2 = val;
#elif defined(__AVR_ATmega328P__)
	OCR2A = val;
#endif
}

void startMotor(void)
{
	uint8_t i; //used for "for" loops

	TIMER1_STOP();
#if defined(__AVR_ATmega8__)
	TIMSK &= ~(1 << OCIE1B);
#elif defined(__AVR_ATmega328P__)
	TIMSK1 &= ~(1 << OCIE1B);
#endif
	
	//lock rotor
	pwmPhase = 1;
	setPWM(STARTUP_LOCK_PWM);
	PWM_START(); //enable PWM
	
	commState = 1;
	
	DISABLE_SPIKE_REJECTION();
	DISABLE_COMM();

	/* previously defined Timer0 OVF didn't work, retry here */
	TCCR1B = 0;
	TCNT1 = 0xFF;
	TIFR1 &= ~(1 << TOV1);
	TIMSK1 |= (1 << TOIE1);
	//sei();
	TCCR1B |= (1 << CS10);

	/* apply a number of commutations to lock the rotor */
	for (i=0; i<2; i++) {
		t1_ovfs = 0;
		nextCommutation();
		while (t1_ovfs < STARTUP_RLOCK) {
		} //wait until defined overflows occurs
	}

	//Commutations now handled in interrupts   
	setPWM(STARTUP_RAMP_PWM);                  
	startupState = 1;
	OCR1A = STARTUP_TICKS_BEGIN;
	TCNT1 = 0;
	ENABLE_COMM();
	while (OCR1A > STARTUP_TICKS_END) {} //Wait until ramp has finished

	
	//Getting ready to detect ZC using analog comparator
	//DISABLE_COMM();
	startupState = 0;

	zcActive = 0;
	ENABLE_SPIKE_REJECTION();
	OCR1B = ZC_BLANKING_TICKS; //Set blanking period to avoid inductive spikes
}

void stopMotor(void)
{
}

void nextCommutation(void)
{
	//When switching low side, extra logic tests are used to make sure if PWM was in an 'on' state,
	//	it will remain that way and visa versa
	
	//To make code more efficient, it is assumed that commState always starts at 0 after a MCU reset,
	//	and direction of rotation can not change during operation.
	
	//TODO: ADD IF STATEMENT FOR DIRECTION OF SPIN
	
	switch (commState) {

	case 1:
		//Do comparator stuff here before switching occurs
		SET_B_HIGH();
		CLR_C_HIGH();
		ZC_DETECT_FALLING();
		zcACO = 1;
		ADMUX = ADC_C;
		break;
	case 2:
		//Change pwmPhase 1->3, check current state
		if (GET_A_LOW()) {
			SET_C_LOW(); //if previous pwmPhase mosfet was on, set new phase to on
			CLR_A_LOW();
		}
		//If mosfet was off, then the new phase will be activated on the next pwm interrupt,
		//	no need to do anything extra
		pwmPhase = 3; //Set pwm to C_LOW
		ZC_DETECT_RISING();
		zcACO = 0;
		ADMUX = ADC_A;
	  	break;
	case 3:
		SET_A_HIGH();
		CLR_B_HIGH();
		ZC_DETECT_FALLING();
		zcACO = 1;
		ADMUX = ADC_B;
	  	break;
	case 4:
		//Change pwmPhase 3->2, check current state
		if (GET_C_LOW()) {
			SET_B_LOW(); //if previous pwmPhase mosfet was on, set new phase to on
			CLR_C_LOW();
		}
		//If mosfet was off, then the new phase will be activated on the next pwm interrupt,
		//	no need to do anything extra
		pwmPhase = 2; //Set pwm to B_LOW
		ZC_DETECT_RISING();
		zcACO = 0;
		ADMUX = ADC_C;
	  	break;
	case 5:
		SET_C_HIGH();
		CLR_A_HIGH();
		ZC_DETECT_FALLING();
		zcACO = 1;
		ADMUX = ADC_A;
	  	break;
	case 6:
		//Change pwmPhase 2->1, check current state
		if (GET_B_LOW()) {
			SET_A_LOW(); //if previous pwmPhase mosfet was on, set new phase to on
			CLR_B_LOW();
		}
		//If mosfet was off, then the new phase will be activated on the next pwm interrupt,
		//	no need to do anything extra
		pwmPhase = 1; //Set pwm to A_LOW
		commState = 0; //Start from beginning
		ZC_DETECT_RISING();
		zcACO = 0;
		ADMUX = ADC_B;
	  	break;
	}
	commState++;
	
	if (!startupState) OCR1B = ZC_BLANKING_TICKS; //Set blanking period
}

void clrAllOutputs(void)
{
	/* flush out any remaining currents */
	SET_A_HIGH();
	SET_A_LOW();
	CLR_A_HIGH();
	CLR_A_LOW();

	SET_B_HIGH();
	SET_B_LOW();
	CLR_B_HIGH();
	CLR_B_LOW();

	SET_C_HIGH();
	SET_C_LOW();
	CLR_C_HIGH();
	CLR_C_LOW();
}

void init_mosfets(void)
{
	//Set mosfet drive pins as outputs
    HIGH_A_DDR |= (1 << HIGH_A);
    HIGH_B_DDR |= (1 << HIGH_B);
    HIGH_C_DDR |= (1 << HIGH_C);
    LOW_A_DDR |= (1 << LOW_A);
    LOW_B_DDR |= (1 << LOW_B);
    LOW_C_DDR |= (1 << LOW_C);
		
	//Turn all mosfets off
	clrAllOutputs();
}

void init_registers(void)
{
	//Set up registers, timers and interrupts

	//Edit the following registers in the header file
	
	//Timer1 - commutation timer
#if defined(__AVR_ATmega8__)
	TIMSK |= TIMER1_TIMSK;
#elif defined(__AVR_ATmega328P__)
	//TIMSK1 |= TIMER1_TIMSK;
	TIMSK1 |= (1 << TOIE1);
#endif
	
	//Timer2 - PWM timer
#if defined(__AVR_ATmega8__)
	TCCR2 = TIMER2_TCCR2;
	TIMSK |= TIMER2_TIMSK;
#elif defined(__AVR_ATmega328P__)
	TCCR2A = TIMER2_TCCR2A;
	TIMSK2 |= TIMER2_TIMSK;
#endif
	
	//Analog comparator settings
#if defined(__AVR_ATmega8__)
	SFIOR |= (1 << ACME); //Set comparator -ve input to ADMUX
#elif defined(__AVR_ATmega328P__)
	ADCSRB |= (1 << ACME); //Set comparator -ve input to ADMUX
#endif
	ACSR |= (1 << ACIC); //enable comparator to trigger input capture
	
	sei(); //Enable interrupts
}

void init_twi(void)
{
	// setting for 100kHz
	TWSR |= (1 << TWPS0) | (1 << TWPS1);
	TWBR = 125;

	TWAR = (TWI_ADDR << 1) | (1 << TWGCE); // enable TWI general call
	TWCR = (1 << TWEA) | (1 << TWEN) | (1 << TWIE);
}
