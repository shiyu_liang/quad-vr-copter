//*****************************************************************************
// rom_pin_mux_config.c
//
// configure the device pins for different signals
//
// Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/ 
// 
// 
//  Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
//    Redistributions of source code must retain the above copyright 
//    notice, this list of conditions and the following disclaimer.
//
//    Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the 
//    documentation and/or other materials provided with the   
//    distribution.
//
//    Neither the name of Texas Instruments Incorporated nor the names of
//    its contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*****************************************************************************

// This file was automatically generated on 2/8/2015 at 10:58:41 AM
// by TI PinMux version 3.0.516 
//
//*****************************************************************************

#include "pin_mux_config.h" 
#include "hw_types.h"
#include "hw_memmap.h"
#include "hw_gpio.h"
#include "pin.h"
#include "gpio.h"
#include "prcm.h"
#include "rom.h"
#include "rom_map.h"

//*****************************************************************************
void PinMuxConfig(void)
{
    //
    // Enable Peripheral Clocks 
    //
    MAP_PRCMPeripheralClkEnable(PRCM_UARTA0, PRCM_RUN_MODE_CLK);
    MAP_PRCMPeripheralClkEnable(PRCM_I2CA0, PRCM_RUN_MODE_CLK);
    MAP_PRCMPeripheralClkEnable(PRCM_TIMERA3, PRCM_RUN_MODE_CLK);
    MAP_PRCMPeripheralClkEnable(PRCM_TIMERA1, PRCM_RUN_MODE_CLK);
    MAP_PRCMPeripheralClkEnable(PRCM_TIMERA2, PRCM_RUN_MODE_CLK);
    MAP_PRCMPeripheralClkEnable(PRCM_CAMERA, PRCM_RUN_MODE_CLK);

    //
    // Configure PIN_52 for UART0 UART0_RTS
    //
    MAP_PinTypeUART(PIN_52, PIN_MODE_6);

    //
    // Configure PIN_50 for UART0 UART0_CTS
    //
    MAP_PinTypeUART(PIN_50, PIN_MODE_12);

    //
    // Configure PIN_62 for UART0 UART0_TX
    //
    MAP_PinTypeUART(PIN_62, PIN_MODE_11);

    //
    // Configure PIN_45 for UART0 UART0_RX
    //
    MAP_PinTypeUART(PIN_45, PIN_MODE_9);

    //
    // Configure PIN_16 for I2C0 I2C_SCL
    //
    MAP_PinTypeI2C(PIN_16, PIN_MODE_9);

    //
    // Configure PIN_17 for I2C0 I2C_SDA
    //
    MAP_PinTypeI2C(PIN_17, PIN_MODE_9);

    //
    // Configure PIN_63 for TimerCP6 GT_CCP06
    //
    MAP_PinTypeTimer(PIN_63, PIN_MODE_12);

    //
    // Configure PIN_57 for TimerCP2 GT_CCP02
    //
    MAP_PinTypeTimer(PIN_57, PIN_MODE_7);

    //
    // Configure PIN_15 for TimerCP4 GT_CCP04
    //
    MAP_PinTypeTimer(PIN_15, PIN_MODE_5);

    //
    // Configure PIN_53 for TimerCP5 GT_CCP05
    //
    MAP_PinTypeTimer(PIN_53, PIN_MODE_4);

    //
    // Configure PIN_55 for Camera0 PIXCLK
    //
    MAP_PinTypeCamera(PIN_55, PIN_MODE_4);

    //
    // Configure PIN_58 for Camera0 CAM_pDATA7
    //
    MAP_PinTypeCamera(PIN_58, PIN_MODE_4);

    //
    // Configure PIN_59 for Camera0 CAM_pDATA6
    //
    MAP_PinTypeCamera(PIN_59, PIN_MODE_4);

    //
    // Configure PIN_60 for Camera0 CAM_pDATA5
    //
    MAP_PinTypeCamera(PIN_60, PIN_MODE_4);

    //
    // Configure PIN_61 for Camera0 CAM_pDATA4
    //
    MAP_PinTypeCamera(PIN_61, PIN_MODE_4);

    //
    // Configure PIN_02 for Camera0 pXCLK
    //
    MAP_PinTypeCamera(PIN_02, PIN_MODE_4);

    //
    // Configure PIN_03 for Camera0 VSYNC
    //
    MAP_PinTypeCamera(PIN_03, PIN_MODE_4);

    //
    // Configure PIN_04 for Camera0 HSYNC
    //
    MAP_PinTypeCamera(PIN_04, PIN_MODE_4);

    //
    // Configure PIN_05 for Camera0 CAM_pDATA8
    //
    MAP_PinTypeCamera(PIN_05, PIN_MODE_4);

    //
    // Configure PIN_06 for Camera0 CAM_pDATA9
    //
    MAP_PinTypeCamera(PIN_06, PIN_MODE_4);

    //
    // Configure PIN_07 for Camera0 CAM_pDATA10
    //
    MAP_PinTypeCamera(PIN_07, PIN_MODE_4);

    //
    // Configure PIN_08 for Camera0 CAM_pDATA11
    //
    MAP_PinTypeCamera(PIN_08, PIN_MODE_4);

    //
    // Configure PIN_01 for TimerPWM6 GT_PWM06
    //
    MAP_PinTypeTimer(PIN_01, PIN_MODE_3);

    //
    // Configure PIN_64 for TimerPWM5 GT_PWM05
    //
    MAP_PinTypeTimer(PIN_64, PIN_MODE_3);

    //
    // Configure PIN_21 for TimerPWM2 GT_PWM02
    //
    MAP_PinTypeTimer(PIN_21, PIN_MODE_9);

    //
    // Configure PIN_19 for TimerPWM3 GT_PWM03
    //
    MAP_PinTypeTimer(PIN_19, PIN_MODE_8);
}
